#!/bin/sh

echo "start testing znew"

#Linux
g++ main.cpp -o main -std=c++98 -Wall -Wextra -pedantic $inc_dir
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

./main
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

rm -f main

g++ main.cpp -o main -std=c++11 -Wall -Wextra -pedantic $inc_dir
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

./main
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

rm -f main

#Windows
i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++98 -Wall -Wextra -pedantic -static-libgcc -static-libstdc++ $inc_dir
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

wine ./main.exe
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

rm -f main.exe

i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++11 -Wall -Wextra -pedantic -static-libgcc -static-libstdc++ $inc_dir
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

wine ./main.exe
if [ $? -ne 0 ]; then
  echo "test znew failed"
  exit 1
fi

rm -f main.exe

exit 0
