
#include <znew>
#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

class TestTwo
{
public:
  TestTwo(zint16 val1, zreal val2): val1_(val1), val2_(val2)
  {
    if (val1 == 0)
      throw ZInvalidArgumentException();
  };
  
  zint16 getVal1() const { return val1_; };
  zreal  getVal2() const { return val2_; };
  
private:
  zint16 val1_;
  zreal  val2_; 
};

class TestThree
{
public:
  TestThree(zreal a, zbool b, zint c) : a_(a), b_(b), c_(c) {};
  
  zreal getA() const { return a_; };
  zbool getB() const { return b_; };
  zint  getC() const { return c_; };
   
private:
  zreal a_;
  zbool b_;
  zint  c_;
};

bool testznew()
{
  {
    zint *pointer = znew<zint>();
    TEST(pointer != Z_NULL_PTR);
    delete pointer;
  }
  
  {
    zint *pointer = znew<zint>(15);
    TEST(*pointer == 15);
    delete pointer;
  }
  
  {
    TestTwo *pointer = znew<TestTwo>(20, 16.0);
    TEST(pointer->getVal1() == 20);
    TEST(pointer->getVal2() == 16.0);
    delete pointer;
  }
  
  {
    try
    {
      TestTwo *pointer = znew<TestTwo>(0, 1.0);
      TEST(false);
      TEST(pointer != Z_NULL_PTR);
    } catch(const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  {
    TestThree *pointer = znew<TestThree>(2.0, true, 26);
    TEST(pointer->getA() == 2.0);
    TEST(pointer->getB());
    TEST(pointer->getC() == 26);    
  }
  
  return true;
}

bool testznewArray()
{
  {
    try
    {
      zint *pointer = znewArray<zint>(-2);
      TEST(false);
      TEST(pointer!=Z_NULL_PTR);
    } catch(const ZBadAllocException&)
    {
      TEST(true);
    }
  }
  
  {
    try
    {
      zint *pointer = znewArray<zint>(0);
      TEST(false);
      TEST(pointer!=Z_NULL_PTR);
    } catch(const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  {    
    zint *pointer = znewArray<zint>(5);
    TEST(pointer != Z_NULL_PTR);
    delete [] pointer;     
  }
  
  return true;
}

bool testznewPlacement()
{  
  try
  {
    zint *pointer = znewPlacement<zint>(Z_NULL_PTR);
    Z_UNUSED_VARIABLE(pointer);
    TEST(false);
  }catch( const ZInvalidArgumentException&)
  {
    TEST(true);
  }
  
  
  {
    zint *pointer = znew<zint>(15);
    zint *p = znewPlacement<zint>(pointer);
    TEST(p==pointer);
    p->~zint();    
    delete pointer;
  }
  
  {
    zint *pointer = znew<zint>(10);
    zint *p = znewPlacement<zint>(pointer, 25);
    TEST(p==pointer);
    TEST(*pointer==25);
    TEST(*p==25);
    p->~zint();
    delete pointer;
  }
  
  {
    TestTwo *pointer = znew<TestTwo>(2, 2.5);
    TestTwo *p = znewPlacement<TestTwo>(pointer, 5, 26);
    TEST(p==pointer);
    TEST(p->getVal1() == 5);
    TEST(p->getVal2() == 26);
    p->~TestTwo();
    
    delete p;
  }
  
  {
    TestThree *pointer = znew<TestThree>(3, true, 28);
    TestThree *p = znewPlacement<TestThree>(pointer, 5, true, 38);
    TEST(pointer==p);
    TEST(p->getA()==5);
    TEST(p->getB());
    TEST(p->getC()==38);
    p->~TestThree();
    delete pointer;
  }
  
  return true;  
}
 
bool test()
{
  return (testznew() && testznewArray() && testznewPlacement());
}
 
int main()
{   
  return ( test() ? 0 : 1 );
}

