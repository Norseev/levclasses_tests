
@echo off

echo "start testing zassert"

set compile_string=g++ main.cpp -o main.exe -Wall -Wextra -pedantic %inc_dir%

call :test_standard "-std=c++11"
if ERRORLEVEL 1 exit /b 1

call :test_standard "-std=c++98"
if ERRORLEVEL 1 exit /b 1

exit /b 0

:test_standard

rem Тестируем включенный Z_ASSERT 
if exist main.exe del main.exe
%compile_string% %1
if ERRORLEVEL 1 (
  echo "test zassert failed"
  exit /b 1
)

main.exe >nul 2>&1
if not %ERRORLEVEL%==3 (
  echo "test zassert failed"
  exit /b 1
)

main.exe text
if ERRORLEVEL 1 (
  echo "test zassert failed"
  exit /b 1
)

del main.exe

rem Тестируем выключенный Z_ASSERT
%compile_string% %1 -DZ_ASSERT_OFF=
if ERRORLEVEL 1 (
  echo "test zassert failed"
  exit /b 1
)

main.exe
if ERRORLEVEL 1 (
  echo "test zassert failed"
  exit /b 1
)

main.exe text
if ERRORLEVEL 1 (
  echo "test zassert failed"
  exit /b 1
)

del main.exe

rem Тестируем Z_STATIC_ASSERT
%compile_string% %1 -DZ_TEST_STATIC_ASSERT= >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zassert failed"
  exit /b 1
)

exit /b 0
