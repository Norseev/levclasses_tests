#!/bin/sh

echo "start testing zassert"

# $1 - имя компилятора
# $2 - имя исполняемого файла
# $3 - wine (если есть)
# $4 - код возврата по assert
# $5 - дополнительные ключи компилятора
# $6 - стандарт
test_standard_once()
{   
  #Тестируем включенный Z_ASSERT
  rm -f $2
  $1 main.cpp -o $2 -Wall -Wextra -pedantic $5 $6 $inc_dir
  if [ $? -ne 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  $3 ./$2 >/dev/null 2>&1  
  if [ $? -ne $4 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  $3 ./$2 text
  if [ $? -ne 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  #Тестируем выключенный Z_ASSERT
  rm -f $2  
  $1 main.cpp -o $2 -Wall -Wextra -pedantic -DZ_ASSERT_OFF $5 $6 $inc_dir
  if [ $? -ne 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  $3 ./$2
  if [ $? -ne 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  $3 ./$2 text
  if [ $? -ne 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
  
  #Тестируем Z_STATIC_ASSERT
  rm -f $2  
  $1 main.cpp -o $2 -Wall -Wextra -pedantic -DZ_TEST_STATIC_ASSERT $5 $6 $inc_dir >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "test zassert failed"
    exit 1
  fi
}

# $1 - имя компилятора
# $2 - имя исполняемого файла
# $3 - wine (если есть)
# $4 - код возврата по assert
# $5 - Дополнительные ключи компилятора
test_once()
{
  test_standard_once "$1" "$2" "$3" "$4" "$5" "-std=c++11"
  test_standard_once "$1" "$2" "$3" "$4" "$5" "-std=c++98"
}

#Linux
test_once "g++" "main" "" "134" ""

#Windows
test_once "i686-w64-mingw32-g++"   "main.exe" "wine" "3" "-static-libgcc -static-libstdc++"
test_once "x86_64-w64-mingw32-g++" "main.exe" "wine" "3" "-static-libgcc -static-libstdc++"

exit 0
