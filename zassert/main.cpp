
#include <zassert>

template <int V> class MyClass
{  
  Z_STATIC_ASSERT_MESSAGE(V!=0, "V is equal zero");
  Z_STATIC_ASSERT_MESSAGE(V>=0, "V is negative");   
};

int main(int argc, char**)
{
  Z_ASSERT(argc > 1);
  
  if(argc != 0)
    Z_ASSERT(argc > 1);
  
#if defined Z_TEST_STATIC_ASSERT
  Z_STATIC_ASSERT(false);
#else
  Z_STATIC_ASSERT(true);
#endif  
  
  MyClass<3> d;
  ((void)d);
  
  return 0;
}
