
#include <zuniqueptr>
#include <znew>

#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

class Multiplicator
{
public:  
  int multiply(int b) const
  {
    return a_ * b;
  };
  
  Multiplicator(int a) : a_(a) {};
  
private:
  int a_;
};


#ifdef TEST_COPY_CONSTRUCTOR
zbool test_copy_constructor(ZUniquePtr<zint> pointer)
{
  return pointer.isArray();
}
#endif 

//Тесты обычного "пустого" указателя
bool testEmptyPtr()
{  
  ZUniquePtr<zint> unique;
  TEST(unique.isEmpty());
  TEST(!unique.isArray());
    
  //call assert      
  //TEST(*unique==0);
  //unique[0] = 18;
      
  return true;  
}

//Тесты "пустого" указателя на массив
bool testEmptyArrayPtr()
{
  ZUniquePtr<zint, true> unique;
  TEST(unique.isEmpty());
  TEST(unique.isArray());
  if(unique)
  {
    TEST(false);
  } else
    TEST(true);
    
  //call assert     
  // TEST(*unique==0);
  // unique[0] = 18;    
  
  return true;  
}

//Тесты обычного указателя
bool testPtr()
{
  //Тесты обычного указателя
  {    
    ZUniquePtr<zint> unique(znew<zint>(15));
    TEST(!unique.isEmpty());
    TEST(!unique.isArray());
    TEST(*unique == 15);
    TEST(*(unique.get()) == 15);    
    TEST(unique);
    
    //call assert
    // unique[0] = 18;      
  }
  
  //Тесты указателя на объект
  {
    ZUniquePtr<Multiplicator> unique(znew<Multiplicator>(3));
    TEST(!unique.isEmpty());
    TEST(unique->multiply(5)==15);
    TEST(unique.get()->multiply(6)==18);
  }
  
  //Тесты указателя на массив
  {
    ZUniquePtr<zint,true> unique(znewArray<zint>(3));
    TEST(!unique.isEmpty());
    TEST(unique.isArray());
    unique[0] = 1;
    unique[1] = 2;
    unique[2] = 3;
    TEST(unique[0]==1);
    TEST(unique);
  }
  
  return true;  
}

//Тесты конструктора и оператора
bool testConstructrosAndOperators()
{
  //Тест конструктора копирования 
#ifdef TEST_COPY_CONSTRUCTOR
  {
    ZUniquePtr<zint> unique;
    TEST(test_copy_constructor(unique));
    TEST(false);
  }
#endif

  //Тест оператора присваивания копированием
#ifdef TEST_OPERATOR
  {
    zuint *pointer = znew<zuint>(5);
    ZUniquePtr<zuint> unique(pointer);
    
    ZUniquePtr<zuint> unique2;
    
    unique2 = unique;
    TEST(false);
  }
#endif 

  return true;
}

//Тесты метода release
bool testReleaseMethod()
{
  ZUniquePtr<int> unique;
  TEST(unique.release()==Z_NULL_PTR);
  TEST(unique.isEmpty());
  
  int *p = znew<int>(5);
  ZUniquePtr<int> unique2(p);
  TEST(!unique2.isEmpty())
  TEST(unique2.release()==p);
  TEST(unique2.isEmpty());  
  delete p;
  
  int *parray = znewArray<int>(5);
  ZUniquePtr<int,true> unique3(parray);
  TEST(!unique3.isEmpty());
  TEST(unique3.release()==parray);
  TEST(unique3.isEmpty());
  delete [] parray;
  
  return true;
}
//Тесты метода reset
bool testResetMethod()
{
  ZUniquePtr<int> unique;
  int *p = znew<int>(10);
  unique.reset(p);
  TEST(*unique==10);
  unique.reset(p);
  TEST(*unique==10);
  unique.reset(unique.release());
  TEST(*unique==10);
  unique.reset(znew<int>(20));
  TEST(*unique==20);
  unique.reset();
  TEST(unique.isEmpty());
  
  ZUniquePtr<int,true> unique2;
  p = znewArray<int>(5);
  unique2.reset(p);
  unique2.reset(p);
  TEST(!unique2.isEmpty());
  unique2.reset(unique2.release());
  TEST(!unique2.isEmpty());
  unique2.reset();
  TEST(unique2.isEmpty());
  
  return true;
}

bool test()
{
  return (testEmptyPtr() && testEmptyArrayPtr() && testPtr() && 
          testConstructrosAndOperators() && testReleaseMethod() && 
          testResetMethod());
}

int main ()
{   
  return (test() ? 0 : 1);
}
