@echo off

echo "start testing zuniqueptr"

rem Тесты для стандарта С++98
g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zuniqueptr failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zuniqueptr failed"
  exit /b 1
)

del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 %inc_dir% -DTEST_COPY_CONSTRUCTOR >nul 2>&1
if %ERRORLEVEL%==0 (  
  echo "test zuniqueptr failed"
  exit /b 1
)

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 %inc_dir% -DTEST_OPERATOR >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zuniqueptr failed"
  exit /b 1
)

rem Тесты для стандарта С++11
g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zuniqueptr failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zuniqueptr failed"
  exit /b 1
)

del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 %inc_dir% -DTEST_COPY_CONSTRUCTOR >nul 2>&1
if %ERRORLEVEL%==0 (  
  echo "test zuniqueptr failed"
  exit /b 1
)

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 %inc_dir% -DTEST_OPERATOR >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zuniqueptr failed"
  exit /b 1
)

exit /b 0