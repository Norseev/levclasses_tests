#ifndef TEST_H
#define TEST_H

#pragma once

#include <stdio.h>

#define TEST(_expr_) if (!(_expr_))                   \
                     {                                \
                       printf(#_expr_": failed\r\n"); \
                       printf("test failed\r\n");     \
                       return false;                      \
                     }
               

#endif
