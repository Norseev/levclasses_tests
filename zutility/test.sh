#!/bin/sh

echo "start testing zutility"

# $1 - имя скомпилированного файла
# $2 - утилита wine, если производится компиляция под Windows
test_once()
{  
  rm -f $1
  $compile_string -std=c++98 $inc_dir
  
  if [ $? -ne 0 ]; then
    echo "test zutility failed"
    exit 1
  fi
  
  $2 ./$1
  if [ $? -ne 0 ]; then
    echo "test zutility failed"
    exit 1
  fi
  
  rm -f $1
  $compile_string -std=c++11 $inc_dir
  
  if [ $? -ne 0 ]; then
    echo "test zutility failed"
    exit 1
  fi
  
  $2 ./$1
  if [ $? -ne 0 ]; then
    echo "test zutility failed"
    exit 1
  fi
  rm -f $1  
}

#Linux
compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic"
test_once main

#Windows
compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic"
test_once main.exe wine

compile_string="x86_64-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic"
test_once main.exe wine

