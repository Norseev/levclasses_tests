
#include <zutility>
#include <ztypes>
#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

typedef zint Z_CREATE_UNIQUE_NAME(type_name_);
typedef zint Z_CREATE_UNIQUE_NAME(type_name_);

bool testZConcatenate()
{
  zint a(0);
  zint b(0);
  zint ab(25);
  TEST(Z_CONCATENATE_(a,b)==25);
  TEST(Z_CONCATENATE(a,b)==25);
  Z_UNUSED_VARIABLE(a);
  Z_UNUSED_VARIABLE(b);
  
  zint a__LINE__(28);
  TEST(Z_CONCATENATE_(a,__LINE__)==28);
  
  zint a25(16);
  TEST(Z_CONCATENATE(a,__LINE__)==16); 
    
  return true;
}

bool testInfinityLoop()
{
  zint i(0);
  Z_INFINITY_LOOP
    if( i < 10 )
      ++i;
    else
      break;
    
  TEST(i==10);
  
  return true;
}

bool testEmptyExpression()
{
  Z_EMPTY_EXPRESSION;
  
  int a = 0;
  for(int i = 0; i < 10; ++i)
    Z_EMPTY_EXPRESSION;
  ++a;
  TEST(a==1);
  
  return true;  
}

bool test()
{
  return (testZConcatenate() && testInfinityLoop() && testEmptyExpression());
}

int main()
{   
  type_name_8 a(0);
  Z_UNUSED_VARIABLE(a);
    
  return (test() ? 0 : 1);
}
