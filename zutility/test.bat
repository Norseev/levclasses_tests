@echo off

echo "start testing zutility"

if exist main.exe del main.exe

g++ main.cpp -o main.exe -std=c++98 -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test zutility failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zutility failed"
  exit /b 1
)
del main.exe

g++ main.cpp -o main.exe -std=c++11 -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test zutility failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zutility failed"
  exit /b 1
)
del main.exe

exit /b 0
