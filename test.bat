
@echo off

set inc_dir=-I"../../lev/Include"

if "%1"=="" (
  for /D %%i in (*) do (
    cd %%i
    call test
    if ERRORLEVEL 1 exit /b 1
    cd ../
  )
) else (
  cd %1 && call test
  if not ERRORLEVEL 1 cd ../
)
