#!/bin/sh

echo "start testing zenvironment"

rm -f main

g++ main.cpp -o main -Wall -Wextra -pedantic $inc_dir -DZ_OS_LINUX -DZ_OS_WINDOWS >/dev/null 2>&1
if [ $? = 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi

g++ main.cpp -o main -Wall -Wextra -pedantic $inc_dir -DZ_CAPACITY_32 -DZ_CAPACITY_64 >/dev/null 2>&1
if [ $? = 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi

g++ main.cpp -o main -Wall -Wextra -pedantic $inc_dir
./main | grep "Linux_64" >/dev/null
if [ $? != 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi
rm -f main

g++ -m32 main.cpp -o main -Wall -Wextra -pedantic $inc_dir
./main | grep "Linux_32" >/dev/null
if [ $? != 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi
rm -f main

i686-w64-mingw32-g++ main.cpp -o main.exe -Wall -Wextra -pedantic $inc_dir
wine ./main.exe | grep "Windows_32" >/dev/null
if [ $? != 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi
rm -f main.exe

x86_64-w64-mingw32-g++ main.cpp -o main.exe -Wall -Wextra -pedantic $inc_dir
wine ./main.exe | grep "Windows_64" >/dev/null
if [ $? != 0 ]; then
  echo "test zenvironment failed"
  exit 1
fi
rm -f main.exe

exit 0
