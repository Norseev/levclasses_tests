#include <zenvironment>
#include <stdio.h>

int main()
{
#if defined Z_OS_WINDOWS && defined Z_CAPACITY_32
  printf("Windows_32\r\n");
  return 0;
#endif

#if defined Z_OS_WINDOWS && defined Z_CAPACITY_64
  printf("Windows_64\r\n");
  return 0;
#endif

#if defined Z_OS_LINUX && defined Z_CAPACITY_32
  printf("Linux_32\r\n");
  return 0;
#endif

#if defined Z_OS_LINUX && defined Z_CAPACITY_64
  printf("Linux_64\r\n");
  return 0;
#endif

  return 1;
}
