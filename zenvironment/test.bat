@echo off

echo "start testing zenvironment"

if exist main.exe del main.exe
g++ main.cpp -o main.exe -Wall -Wextra -pedantic %inc_dir% -DZ_OS_WINDOWS -DZ_OS_LINUX >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zenvironment failed"
  exit /b 1
)

g++ main.cpp -o main.exe -Wall -Wextra -pedantic %inc_dir% -DZ_CAPACITY_32 -DZ_CAPACITY_64 >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zenvironment failed"
  exit /b 1
)

g++ main.cpp -o main.exe -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test zenvironment failed"
  exit /b 1
)
main.exe | find "Windows_64" >nul
if ERRORLEVEL 1 (
  echo "test zenvironment failed"
  exit /b 1
)
del main.exe

exit /b 0
