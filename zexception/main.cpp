
#include <ztypes>
#include <zexception>

#include "../test.h"

#include <cassert>
#include <wchar.h>
#include <string.h>

using namespace Z_GLOBAL_NAMESPACE;

zbool isEqual(const zunichar *str1, const zunichar *str2)
{
  if(sizeof(zunichar)==sizeof(wchar_t))
    return (wcscmp(reinterpret_cast<const wchar_t*>(str1),
                   reinterpret_cast<const wchar_t*>(str2)) == 0);

  assert(sizeof(zunichar)==1);
  return (strcmp(reinterpret_cast<const char*>(str1),
                 reinterpret_cast<const char*>(str2)) == 0);
}

bool test()
{
  try
  {
    throw ZMessageExceptionBase(Z_TEXT("Hello, I am message"));
  }catch(const ZMessageExceptionBase &exception)
  {
    TEST(isEqual(exception.getMessage(), Z_TEXT("Hello, I am message")));
  }

  try
  {
    throw ZNoImplementException(Z_TEXT("FreeBSD"));
  }catch(const ZMessageExceptionBase &exception)
  {
    TEST(isEqual(exception.getMessage(), Z_TEXT("FreeBSD")));
  }
  return true;
}

int main()
{
  return (test() ? 0 : 1);
}
