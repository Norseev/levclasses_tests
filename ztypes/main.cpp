
#include <ztypes>

#include <string.h>

#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

zuint getLen(const zchar *str)
{
  return strlen(str);
}

bool test()
{
  TEST(sizeof(zint8)==1);
  TEST(sizeof(zuint8)==1);

  TEST(sizeof(zint16)==2);
  TEST(sizeof(zuint16)==2);

  TEST(sizeof(zint32)==4);
  TEST(sizeof(zuint32)==4);

  TEST(sizeof(zint64)==8);
  TEST(sizeof(zuint64)==8);

#if defined Z_CAPACITY_32
  TEST(sizeof(zint)==4);
  TEST(sizeof(zuint)==4);
#elif defined Z_CAPACITY_64
  TEST(sizeof(zint)==8);
  TEST(sizeof(zuint)==8);
#else
  return false;
#endif

  TEST(sizeof(zchar)==1);
  TEST(sizeof(zchar)==sizeof(char));

  TEST(getLen("Hello")==5);
  
  TEST(sizeof(zchar8)==1);
  TEST(sizeof(zchar16)==2);
  TEST(sizeof(zchar32)==4);

  return true;  
}

int main()
{
  return (test() ? 0 : 1);
}
