#!/bin/sh

echo "start testing zstandardc"

compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -Werror $inc_dir"

# $1 - имя скомпилированного файла
# $2 - ключ компиляции под нужный стандарт
# $3 - строка, выводимая программой для нужного стандарта
# $4 - утилита wine, если осуществляется компиляция под Windows
test_standard()
{   
  rm -f $1 
  $compile_string $2
  
  if [ $? != 0 ]; then
    echo "test zstandardc failed"
    exit 1
  fi
  
  $4 ./$1 | grep $3 > /dev/null
  if [ $? != 0 ]; then
    echo "test zstandardc failed"
    exit 1
  fi
  
  $4 ./$1 > /dev/null
  if [ $? != 0 ]; then    
    echo "test zstandardc failed"
    exit 1
 fi
}

test_standard main -std=c++98 "Standard_98"
test_standard main -std=c++11 "Standard_11"
test_standard main -std=c++14 "Standard_14"

compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic -Werror $inc_dir"

test_standard main.exe -std=c++98 "Standard_98" wine
test_standard main.exe -std=c++11 "Standard_11" wine
test_standard main.exe -std=c++14 "Standard_14" wine

compile_string="x86_64-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic -Werror $inc_dir"

test_standard main.exe -std=c++98 "Standard_98" wine
test_standard main.exe -std=c++11 "Standard_11" wine
test_standard main.exe -std=c++14 "Standard_14" wine

rm -f main
rm -f main.exe

exit 0

