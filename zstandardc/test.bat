
@echo off

echo "start testing zstandardc"

set compile_string=g++ main.cpp -o main.exe -Wall -Wextra -pedantic -Werror %inc_dir%

call :test_standard "-std=c++98" "Standard_98"
if ERRORLEVEL 1  exit /b 1

call :test_standard "-std=c++11" "Standard_11"
if ERRORLEVEL 1  exit /b 1

call :test_standard "-std=c++14" "Standard_14"
if ERRORLEVEL 1  exit /b 1

call :test_standard "-std=c++17" "Standard_17"
if ERRORLEVEL 1  exit /b 1

if exist main.exe del main.exe 
exit /b 0

rem %1 - ключ компиляции под нужный стандарт
rem %2 - ожидаемая строка
:test_standard

if exist main.exe del main.exe 

%compile_string% %1

if ERRORLEVEL 1 (
  echo "test zstandardc failed"
	exit /b 1
)

main.exe | find %2 >nul
if ERRORLEVEL 1 (
  echo "test zstandardc failed"
	exit /b 1
)

main.exe >nul
if ERRORLEVEL 1 (
  echo "test zstandardc failed"
	exit /b 1
) else (
  exit /b 0
)
