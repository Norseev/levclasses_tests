#include <zstandardc>
#include <zutility>
#include <stdio.h>
#include <string>
#include <vector>
#include <algorithm>

#include "../test.h"

template <typename T>
  void test_noexcept_expression() Z_NOEXCEPT_EXPRESSION(noexcept(T())) {}

class TestOverrideBase
{
public:
  virtual void doSomething() = 0;  
};

class TestOverride : public TestOverrideBase
{
public:
  virtual void doSomething() Z_OVERRIDE { }
};

class TestConstexprMethod
{
public:
  Z_CONSTEXPR_METHOD TestConstexprMethod(int value) : value_(value) {}
private:
  const int value_;
};

class TestFinalMethod
{
public:
  virtual void doSomething() Z_FINAL_METHOD {} 
};

class TestFinalClass Z_FINAL_CLASS {};

class TestDeletedMethod
{
public:
  void deletedMethod() Z_DELETED_METHOD;
};

class TestDefaultImplementation
{
public:
  TestDefaultImplementation() Z_DEFAULT_IMPLEMENTATION;
  TestDefaultImplementation(int) {}  
};

template <typename T>
  void testDeletedFunction() {}

template <> void testDeletedFunction <bool> () Z_DELETED_FUNCTION;

class TestExplicitOperator
{
public:
  Z_EXPLICIT_OPERATOR operator bool () const Z_NOEXCEPT { return true; }
};

Z_DEPRECATED void deprecatedFunction() {}

class Z_DEPRECATED DeprecatedClass {};

enum Z_DEPRECATED DeprecatedEnum { DEPRECATED_VALUE_1 };

Z_DEPRECATED typedef int deprecated_int_typedef;

template <typename T> class DeprecatedTemplate;
template <> class Z_DEPRECATED DeprecatedTemplate<int> {};

Z_DEPRECATED_MESSAGE("deprecatedFunction2") void deprecatedFunction2() {}

class Z_DEPRECATED_MESSAGE("DeprecatedClass2") DeprecatedClass2 {};

enum Z_DEPRECATED_MESSAGE("DeprecatedEnum2") DeprecatedEnum2 { DEPRECATED_VALUE_2 };

Z_DEPRECATED_MESSAGE("deprecated_int_typedef2") typedef int deprecated_int_typedef2;

template <typename T> class DeprecatedTemplate2;
template <> class Z_DEPRECATED_MESSAGE("DeprecatedTemplate2") DeprecatedTemplate2<int> {};

bool test_noexcept() Z_NOEXCEPT
{
  return true;
}

Z_CONSTEXPR_FUNCTION int mult2(int value) Z_NOEXCEPT
{  
  return value * 2;
}

Z_NORETURN void test_noreturn()
{
  throw 5;
}

void test_fallthrough(int value)
{
  switch(value)
  {
    case 0: printf("0\r\n");
            break;
    
    case 1: printf("1\r\n");
            Z_FALLTHROUGH;
    
    case 2: printf("2\r\n");
            break;
            
    default: printf("default\r\n");
  }   
}

int Z_NODISCARD_FUNCTION testNoDiscardFunction()
{
  return 0;
}

class Z_NODISCARD_CLASS TestNoDiscardClass {};

enum Z_NODISCARD_ENUM TestNoDiscardEnum { NODISCARD_VALUE_1, NODISCARD_VALUE_2};

bool testZMove()
{   
  std::string str("Hello");
  std::vector<std::string> strings;
  
  strings.push_back(str);
  TEST(!str.empty());
  
  strings.push_back(Z_MOVE(str));
  TEST(strings.size()==2);

#ifdef Z_COMPILER_SUPPORT_MOVE_SEMANTICS
  TEST(str.empty());
#endif
  
  return true;
}

bool testZConstexpr()
{
  Z_CONSTEXPR_VARIABLE int val2 = mult2(5);
  TEST(val2==10);
  return true;
}

#ifdef Z_COMPILER_SUPPORT_SCOPED_ENUM
 enum class ScopedEnum { ENUM_VALUE_1, ENUM_VALUE_2 };
#else
 enum ScopedEnum { ENUM_VALUE_1, ENUM_VALUE_2 };
#endif

#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_CLASS
template <typename... Values> class MyClass {};
#endif

void testVariadicClassess()
{
#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_CLASS
  MyClass<> m0;  
  MyClass<int> m1;  
  MyClass<int,double> m2;  
  MyClass<int,double,bool> m3;
  
  Z_UNUSED_VARIABLE(m0);
  Z_UNUSED_VARIABLE(m1);
  Z_UNUSED_VARIABLE(m2);
  Z_UNUSED_VARIABLE(m3);
#endif
}

#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_FUNCTION
 template <typename... Values> void foo( Values...) Z_NOEXCEPT {}
#endif

void testVariadicFunctions()
{
#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_FUNCTION
  foo();
  foo(5);
  foo(5, 2.0);
  foo(5, 2.0, false);
#endif
}

class TestVariadicMethod
{
public:
#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_METHOD
  template <typename... Args> void foo (Args...) const Z_NOEXCEPT {}
#endif  
};

void testVariadicMethods()
{ 
  TestVariadicMethod a;
#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_METHOD
  a.foo();
  a.foo(5);
  a.foo(5, 2.0);
  a.foo(5, 2.0, true);  
#endif
  Z_UNUSED_VARIABLE(a);
}

#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_FUNCTION
  template<typename... Args> void bar(Args...) {}
  
  #ifdef Z_COMPILER_SUPPORT_VARIADIC_MOVE_SEMANTICS
    template <typename... Args> void foo2(Args&&... args)
    {
      bar(std::forward<Args>(args)...);      
    }
  #else
   template <typename... Args> void foo2(Args... args)
   {
      bar(args...);
   }
  #endif  
#endif

void testVariadicMoveSemantics()
{
#ifdef Z_COMPILER_SUPPORT_VARIADIC_TEMPLATE_FUNCTION
  foo2();
  foo2(5);
  foo2(5, 2.0);
  foo2(5, 2.0, true);
#endif
}

#ifdef Z_COMPILER_SUPPORT_VARIADIC_MACROS
  #define VARIADIC_MACROS(...) Z_EMPTY_EXPRESSION
#endif

void testVariadicMacroses()
{
#ifdef Z_COMPILER_SUPPORT_VARIADIC_MACROS
  VARIADIC_MACROS();
  VARIADIC_MACROS("");
  VARIADIC_MACROS("",5);
  VARIADIC_MACROS("",5,true);
#endif
}

#ifdef Z_COMPILER_SUPPORT_TEMPLATE_VARIABLE
  template <typename T> const T PI;

  template <> double PI<double> = 3.14159;
  template <> int    PI<int>    = 3;
  template <> char   PI<char>   = '3';
#endif

Z_INLINE_FUNCTION int testinlineFunction()
{
  return 5;
}

void testChar16Type()
{
#ifdef Z_COMPILER_SUPPORT_CHAR16_TYPE
  char16_t value;
  Z_UNUSED_VARIABLE(value);
#endif
}

void testChar32Type()
{
#ifdef Z_COMPILER_SUPPORT_CHAR32_TYPE
  char32_t value;
  Z_UNUSED_VARIABLE(value);
#endif
}

#ifdef Z_COMPILER_SUPPORT_INITIALIZER_LIST
  template <typename T> void initializerList( std::initializer_list<T> )
  { 
  }
#endif

void testInitializerList()
{
#ifdef Z_COMPILER_SUPPORT_INITIALIZER_LIST
  initializerList({1, 2, 3});
#endif  
}

void testLambdaExpression()
{
  std::vector<int> numbers;
  numbers.push_back(1);
  numbers.push_back(8);
  numbers.push_back(5);
  numbers.push_back(14);

#ifdef Z_COMPILER_SUPPORT_LAMBDA_EXPRESSION  
  std::for_each(numbers.begin(), numbers.end(), [](int)
  { 
    Z_EMPTY_EXPRESSION;
  });
#endif
}

void testStaticAssert()
{
#ifdef Z_COMPILER_SUPPORT_STATIC_ASSERT
  static_assert(true);
#endif

#ifdef Z_COMPILER_SUPPORT_STATIC_ASSERT_MESSAGE
  static_assert(true, "need be true");
#endif
}

void testNullPtr()
{
#ifdef Z_COMPILER_SUPPORT_NULLPTR
  int *p = nullptr;
  Z_UNUSED_VARIABLE(p);
#endif
}

void testu8Prefix()
{
#ifdef Z_COMPILER_SUPPORT_U8_PREFIX
  const char test[] = u8"hello";
  Z_UNUSED_VARIABLE(test);
#endif
}

void testUCapitalPrefix()
{
#ifdef Z_COMPILER_SUPPORT_U_CAPITAL_PREFIX
  const char *p = reinterpret_cast<const char*>(U"Hello");
  Z_UNUSED_VARIABLE(p);
#endif
}

void testUSmallPrefix()
{
#ifdef Z_COMPILER_SUPPORT_U_SMALL_PREFIX
  const char *p = reinterpret_cast<const char*>(u"Hello");
  Z_UNUSED_VARIABLE(p);
#endif
}

int main()
{

//printf("%d\r\n", (int)(__cplusplus));

  Z_ATTRIBUTE_NO_USE typedef int mytype;

#ifdef Z_COMPILER_SUPPORT_AUTO
  auto auto_variable = 56;
  Z_UNUSED_VARIABLE(auto_variable);
#endif

#ifdef Z_COMPILER_SUPPORT_SCOPED_ENUM
  ScopedEnum enumValue = ScopedEnum::ENUM_VALUE_1;
  Z_UNUSED_VARIABLE(enumValue);
#else
  ScopedEnum enumValue = ENUM_VALUE_1;
  Z_UNUSED_VARIABLE(enumValue);
#endif  
      
  if(!test_noexcept())
    return 1;  
    
  
  if(!testZConstexpr())
    return 1;
    
  TestDefaultImplementation testDefaultImplementation;
  Z_UNUSED_VARIABLE(testDefaultImplementation);  
    
  TestExplicitOperator testExplicitOperator;
  if(testExplicitOperator)
    Z_EMPTY_EXPRESSION;
    
#if Z_STANDARD_MORE_17
  printf("Standard_17\r\n");
  return (testZMove() ? 0 : 1);
#endif

#if Z_STANDARD_MORE_14
  printf("Standard_14\r\n");
  return (testZMove() ? 0 : 1); 
#endif

#if Z_STANDARD_MORE_11
  printf("Standard_11\r\n");
  return (testZMove() ? 0 : 1); 
#endif

#if Z_STANDARD_MORE_98
  printf("Standard_98\r\n");
  return (testZMove() ? 0 : 1); 
#endif

  printf("Standard is not defined\r\n");

  return 1;
}

