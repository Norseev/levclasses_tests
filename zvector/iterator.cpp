//I_ASSERT == 0  operator *
//I_ASSERT == 1  operator ->
//I_ASSERT == 2  operator [] (zint > 0)
//I_ASSERT == 3  operator [] (zint < 0)
//I_ASSERT == 4  operator ++
//I_ASSERT == 5  operator ++ (int)
//I_ASSERT == 6  operator --
//I_ASSERT == 7  operator -- (int)
//I_ASSERT == 8  operator += (zint > 0)
//I_ASSERT == 9  operator += (zint < 0)
//I_ASSERT == 10 operator -= (zint > 0)
//I_ASSERT == 11 operator -= (zint < 0)
//I_ASSERT == 12 operator +  (zint > 0)
//I_ASSERT == 13 operator +  (zint < 0)
//I_ASSERT == 14 operator -  (zint > 0)
//I_ASSERT == 15 operator -  (zint < 0)
//I_ASSERT == 16 operator -  (Iterator, Iterator)
//I_ASSERT == 17 operator ==
//I_ASSERT == 18 operator !=
//I_ASSERT == 19 operator <
//I_ASSERT == 20 operator >
//I_ASSERT == 21 operator <=
//I_ASSERT == 22 operator >=

#include <zvector>
#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

class MultClass
{
public:
  zint mult(zint param) { return param*koeff_; };
  zint mult2(zint param) const {return param; };
  
  explicit MultClass(zint koeff) : koeff_(koeff) {};
  
private:
  zint koeff_;
};


int main ()
{
#ifdef EMPTY
  //Тесты пустого итератора
  {
  #if defined I_ASSERT && I_ASSERT == 0
    ZVector<zint>::Iterator i;
    TEST(*i==0);
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 1
    ZVector<MultClass>::Iterator i;
    TEST(i->mult(2)==4);
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 2
    ZVector<zint>::Iterator i;
    TEST(i[1]==0);
    TEST(false);
  #endif  
  
  #if defined I_ASSERT && I_ASSERT == 3
    ZVector<zint>::Iterator i;
    TEST(i[-2]==0);
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 4
    ZVector<zint>::Iterator i;
    ++i;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 5
    ZVector<zint>::Iterator i;
    i++;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 6
    ZVector<zint>::Iterator i;
    --i;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 7
    ZVector<zint>::Iterator i;
    i--;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 8
    ZVector<zint>::Iterator i;
    i+=1;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 9
    ZVector<zint>::Iterator i;
    i+=-1;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 10
    ZVector<zint>::Iterator i;
    i-=1;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 11
    ZVector<zint>::Iterator i;
    i-=-1;
    TEST(false);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 12
    ZVector<zint>::Iterator i;
    ZVector<zint>::Iterator i2 = i + 1;
    TEST(false);
    TEST(*i2==0);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 13
    ZVector<zint>::Iterator i;
    ZVector<zint>::Iterator i2 = i + (-1);
    TEST(false);
    TEST(*i2==0);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 14
    ZVector<zint>::Iterator i;
    ZVector<zint>::Iterator i2 = i - 1;
    TEST(false);
    TEST(*i2==0);
  #endif
  
  #if defined I_ASSERT && I_ASSERT == 15
    ZVector<zint>::Iterator i;
    ZVector<zint>::Iterator i2 = i - (-1);
    TEST(false);
    TEST(*i2==0);
  #endif
  }
#endif      
//Тесты простого итератора
  {    
    ZVector<zint> vec;
    vec << 15 << 25 << 60;
    TEST(*(vec.begin())==15);
    
    ZVector<zint>::Iterator it = vec.begin();
    TEST(*it==15);
    
    ZVector<zint>::Iterator it_end = vec.end();
    TEST(it!=it_end)
    
    *it=30;
    TEST(vec.at(0)==30);
    
    #if defined I_ASSERT && I_ASSERT == 0
      
      TEST(*it_end==0);
      TEST(false);
    #endif
    
    it[0]=80;
    TEST(it[0]==80);
    TEST(it[2]==60);
    TEST(vec.end()[-1]==60);
    
    #if defined I_ASSERT && I_ASSERT == 2
      TEST(it[5]==0);
      TEST(false);
    #endif
  
    #if defined I_ASSERT && I_ASSERT == 3  
      TEST(it[-5]==0);
      TEST(false);
    #endif
        
    TEST(*(++it)==25);
    TEST(*it==25);
    TEST(*(it++)==25);
    TEST(*it==60);
    
    #if defined I_ASSERT && I_ASSERT == 4
      ++it_end;
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 5
      it_end++;
      TEST(false);
    #endif
    
    TEST(*(--it)==25);
    TEST(*it==25);
    TEST(*(it--)==25);
    TEST(*it==80);
    
    #if defined I_ASSERT && I_ASSERT == 6
      --(vec.begin());
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 7
      (vec.begin())--;
      TEST(false);
    #endif
    
    TEST(*(it+=2)==60);
    TEST(*it==60);
    
    #if defined I_ASSERT && I_ASSERT == 8
      it_end+=2;
      TEST(false);
    #endif
    
    TEST(*(it+=-2)==80);
    TEST(*it==80);
    
    #if defined I_ASSERT && I_ASSERT == 9
      it+=-10;
      TEST(false);
    #endif
    
    TEST(*(it-=-2)==60);
    TEST(*it==60);
        
    TEST(*(it-=2)==80);
    TEST(*it==80);
    
    #if defined I_ASSERT && I_ASSERT == 10
      it-=10;
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 11
      it_end-=-2;
      TEST(false);
    #endif
    
    TEST(*(it+2)==60);
    TEST(*(vec.end()+(-2))==25);
    
    #if defined I_ASSERT && I_ASSERT == 12
      it_end + 2;
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 13
      it + (-5);
      TEST(false);
    #endif
    
    TEST(*(vec.end()-2)==25);
    TEST(*(it-(-2))==60);
    
    #if defined I_ASSERT && I_ASSERT == 14
      it - 10;
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 15
      it_end - (-1);
      TEST(false);
    #endif
    
    TEST((vec.end()-it)==3);
    TEST((it-vec.end())==-3);
    
    #if defined I_ASSERT && I_ASSERT == 16
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it - vec2.begin();
      TEST(false);
    #endif
    
    TEST(it==vec.begin());
    TEST(it!=vec.end());
    it+=3;
    TEST(it==vec.end());
    TEST(it!=vec.begin());
    
    #if defined I_ASSERT && I_ASSERT == 17
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it == vec2.begin();
      TEST(false);
    #endif
    
    #if defined I_ASSERT && I_ASSERT == 18
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it != vec2.begin();
      TEST(false);
    #endif
    
    it-=3;
    TEST(it<it_end);
    TEST(!(it<it));
    TEST(!(it_end<it));
    
    #if defined I_ASSERT && I_ASSERT == 19
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it < vec2.begin();
      TEST(false);
    #endif
    
    TEST(it_end>it);
    TEST(!(it>it));
    TEST(!(it>it_end));
    
    #if defined I_ASSERT && I_ASSERT == 20
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it > vec2.begin();
      TEST(false);
    #endif
    
    
    TEST(it<=it_end);
    TEST(it<=it);
    TEST(!(it_end<=it));
    
    #if defined I_ASSERT && I_ASSERT == 21
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it <= vec2.begin();
      TEST(false);
    #endif
    
    TEST(it_end>=it);
    TEST(it>=it);
    TEST(!(it>=it_end));
    
    #if defined I_ASSERT && I_ASSERT == 22
      ZVector<zint> vec2;
      vec2 << 1 << 2 << 3;
      it >= vec2.begin();
      TEST(false);
    #endif
    
    zint sum = 0;
    for(ZVector<zint>::Iterator i = vec.begin(); i != vec.end(); ++i)
      sum += *i;
    TEST(sum==165);
  }

//Тесты оператора -> итератора
#ifndef I_ASSERT
  {
    ZVector<MultClass> vec;
    vec << MultClass(2) << MultClass(3);
    TEST(vec.begin()->mult(15)==30);
    
    ZVector<MultClass>::Iterator it = vec.begin();
    TEST(it->mult(18)==36);    
  }
#else
  {
    ZVector<MultClass> vec;
    vec << MultClass(2) << MultClass(3);
    TEST(vec.begin()->mult(15)==30);
    
    ZVector<MultClass>::Iterator it = vec.begin();
    TEST(it->mult(18)==36);    
    
    #if I_ASSERT == 1
      ZVector<MultClass>::Iterator it_end = vec.end();
      TEST(it_end->mult(10)==20);
      TEST(false);
    #endif    
  }
#endif
 
 
 return 0; 
}
