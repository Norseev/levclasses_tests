#!/bin/sh

echo "start testing zvector (no tests)"

exit 0

#export inc_dir=-I"../../lev/Include"

# $1 - имя скомпилированного файла
# $2 - утилита wine, если производится компиляция под Windows
test_once()
{
  rm -f $1
  $compile_string

  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi

  $2 ./$1
  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi
  
  rm -f $1
  
  $compile_string -DZ_USE_FAST_ITERATORS

  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi

  $2 ./$1
  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi
  
  rm -f $1    
}

test_const_iterator_compiler()
{
  $compile_string -DTRY_CHANGE= >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "test zvector failed (TRY_CHANGE)"
    return 1
  fi
  
  $compile_string -DTRY_CHANGE2= >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "test zvector failed (TRY_CHANGE2)"
    return 1
  fi
  
  return 0
}
#1 -  имя итератора
test_const_iterator()
{
  rm -f $1
  rm -f $1.exe
  
  compile_string="g++ $1.cpp -o $1 -Wall -Wextra -pedantic $inc_dir"
  test_const_iterator_compiler
  if [ $? -ne 0 ]; then
    return 1
  fi
  
  compile_string="g++ -m32 $1.cpp -o $1 -Wall -Wextra -pedantic $inc_dir"
  test_const_iterator_compiler
  if [ $? -ne 0 ]; then
    return 1
  fi
  
  compile_string="x86_64-w64-mingw32-g++ $1.cpp -o $1 -Wall -Wextra -pedantic -static-libgcc -static-libstdc++ $inc_dir"
  test_const_iterator_compiler
  if [ $? -ne 0 ]; then
    return 1
  fi
  
  compile_string="i686-w64-mingw32-g++ $1.cpp -o $1 -Wall -Wextra -pedantic -static-libgcc -static-libstdc++ $inc_dir"
  test_const_iterator_compiler
  if [ $? -ne 0 ]; then
    return 1
  fi
  
  return 0
}

#Linux 64-bit
compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -std=c++98 $inc_dir"
test_once main
if [ $? -ne 0 ]; then
  exit 1
fi

compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -std=c++11 $inc_dir"
test_once main
if [ $? -ne 0 ]; then
  exit 1
fi

#Linux 32 bit
compile_string="g++ main.cpp -o main -m32 -Wall -Wextra -pedantic -std=c++98 $inc_dir"
test_once main
if [ $? -ne 0 ]; then
  exit 1
fi

compile_string="g++ main.cpp -o main -m32 -Wall -Wextra -pedantic -std=c++11 $inc_dir"
test_once main
if [ $? -ne 0 ]; then
  exit 1
fi

rm -f main

#Windows
compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++98 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? -ne 0 ]; then
  exit 1
fi

compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++11 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? -ne 0 ]; then
  exit 1
fi

rm -f main.exe

./test_iterator.sh "iterator"
if [ $? -ne 0 ]; then
  exit 1
fi

./test_iterator.sh "const_iterator"
if [ $? -ne 0 ]; then
  exit 1
fi
test_const_iterator "const_iterator"
if [ $? -ne 0 ]; then
  exit 1
fi

./test_iterator.sh "reverse_iterator"
if [ $? -ne 0 ]; then
  exit 1
fi

./test_iterator.sh "const_reverse_iterator"
if [ $? -ne 0 ]; then
  exit 1
fi
test_const_iterator "const_reverse_iterator"
if [ $? -ne 0 ]; then
  exit 1
fi

exit 0
