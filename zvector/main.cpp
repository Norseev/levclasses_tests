#include <zvector>

#include "../test.h"
#include <algorithm> 

using namespace Z_GLOBAL_NAMESPACE;

class Two
{
public:
  Two(zint first, zint second) : first_(first), second_(second) {};
  
  zint getFirst()  const { return first_;  }
  zint getSecond() const { return second_; }    
  
private:
  zint first_;
  zint second_;  
};

class Three
{
public:
  Three(zint first, zint second, zint third) :
    first_(first), second_(second), third_(third) {};
  
  zint getFirst()  const { return first_ ; }
  zint getSecond() const { return second_; }
  zint getThird()  const { return third_ ; }
  
private:
  zint first_;
  zint second_;
  zint third_;
};

zbool operator == (const Two &left, const Two &right)
{
  return (left.getFirst()  == right.getFirst() &&
          left.getSecond() == right.getSecond()   );
}

zbool operator == (const Three &left, const Three &right)
{
  return (left.getFirst()  == right.getFirst()  &&
          left.getSecond() == right.getSecond() &&
          left.getThird()  == right.getThird()     );
}

zint computeSum(const ZVector<zint> &vec)
{
  zint sum = 0;
  for(ZVector<zint>::ConstIterator i = vec.begin(); i != vec.end(); ++i)
    sum += *i;
  
  TEST(vec.at(0)==5);
  TEST(vec[0]==5);
      
  return sum;
}

int main ()
{  
  //Тесты пустого вектора
  {
    ZVector<zint> vec;
    TEST(vec.isEmpty());
    TEST(vec.getSize()==0);
  }
  
  //Тесты непустого вектора
  {
    ZVector<zint> vec;
    vec.pushBack(5).pushBack(2).pushBack(15);
    
    TEST(!vec.isEmpty());
    TEST(vec.getSize()==3);
    TEST(vec.at(0)==5);
    TEST(vec.at(1)==2);
    TEST(vec.at(2)==15);
    
    TEST(vec[0]==5);
    TEST(vec[1]==2);
    TEST(vec[2]==15);
    
    vec.at(1) = 20;
    TEST(vec[1]==20);
    
    vec[2] = 150;
    TEST(vec.at(2)==150);
    
    TEST(computeSum(vec)==175);
    
    try
    {
      TEST(vec.at(3)==0);
      TEST(false);
    } catch(const ZOutOfRangeException&)
    {
      TEST(true);
    }
    
    try
    {
      TEST(vec[4]==0);
      TEST(false);
    } catch(const ZOutOfRangeException&)
    {
      TEST(true);
    }
    
    vec.clear();
    TEST(vec.isEmpty());    
  }
  
  //Тесты списка инициализации
#if Z_STANDARD_MORE_11
  {
    ZVector<zint> vec {1, 5, 8};
    TEST(vec.getSize()==3);
    TEST(vec[0]==1);
    TEST(vec[1]==5);
    TEST(vec[2]==8);
  }
#endif
  
  //Тесты метода resize
  {
    ZVector<zint> vec;    
    vec << 1 << 2 << 3;
    TEST(vec.getSize()==3);
    TEST(vec[0]==1);
    TEST(vec[1]==2);
    TEST(vec[2]==3);
    
    vec.resize(5);
    TEST(vec.getSize()==5);
    
    vec.resize(2);
    TEST(vec.getSize()==2);
    
    try
    {
      vec.resize(-1);
      TEST(false);
    } catch (const ZBadAllocException&)
    {
      TEST(true);
    }
    
    TEST(vec.getSize()==2);
    vec.resize(5, 3);
    TEST(vec.at(3)==3);
    TEST(vec.at(4)==3);    
  }
  //Тесты методов reserve и getCapacity
  {
    ZVector<zint> vec;
    vec.reserve(20);
    TEST(vec.getCapacity()==20);
    
    try
    {
      vec.reserve(-5);
      TEST(false); 
    } catch (const ZBadAllocException&)
    {
      TEST(true);
    }
    
    vec.clear();
    TEST(vec.getCapacity()==20);
  }
  
  //Тесты конструктора, задающего ёмкость
  {
    ZVector<zint> vec(15);
    TEST(vec.isEmpty());
    TEST(vec.getCapacity()==15);
    
    try
    {
      ZVector<zint> vec1(-5);
      TEST(vec1.isEmpty());
      TEST(false);
    } catch( const ZBadAllocException&)
    {
      TEST(true);
    }    
  }
   
  //Тесты конструктора перемещения
  #if Z_STANDARD_MORE_11
  {
    ZVector<zint> vec;
    vec << 10 << 20 << 30;
    TEST(vec.getSize()==3);
    ZVector<zint> vec2(std::move(vec));
    TEST(vec.isEmpty());
    TEST(vec2.getSize()==3);
  }
  #endif
  //Тесты конструктора копирования на основе std::vector
  {
    std::vector<zint> vec;
    vec.push_back(10); vec.push_back(20); vec.push_back(30);
    ZVector<zint> vec2(vec);
    TEST(vec2.getSize()==3);
    TEST(vec2.at(0)==10);
    TEST(vec2.at(1)==20);
    TEST(vec2.at(2)==30);
  }
  //Тесты конструктора перемещения на основе std::vector
  #if Z_STANDARD_MORE_11
  {
    std::vector<zint> vec {10, 20, 30};    
    ZVector<zint> vec2(std::move(vec));
    TEST(vec.empty());
    TEST(vec2.getSize()==3);
    TEST(vec2.at(0)==10);
    TEST(vec2.at(1)==20);
    TEST(vec2.at(2)==30);
  }
  #endif
  
  //Тесты оператора присваивания копированием
  {
    ZVector<zint> vec1;
    vec1 << 10 << 20 << 30;
    
    ZVector<zint> vec2;
    TEST(vec2.isEmpty());
    
    vec2 = vec1;
    
    TEST(vec1.getSize()==3);
    TEST(vec2.getSize()==3);
  }
    
  //Тесты оператора присваивания перемещением
  #if Z_STANDARD_MORE_11
  {
    ZVector<zint> vec1 { 10, 20, 30 };
        
    ZVector<zint> vec2;
    TEST(vec2.isEmpty());
    
    vec2 = std::move(vec1);
    
    TEST(vec1.isEmpty());
    TEST(vec2.getSize()==3);
  }
  #endif
  
  //Тесты оператора присваивания копированием для std::vector
  {
    std::vector<zint> vec1;
    vec1.push_back(10); vec1.push_back(20); vec1.push_back(30);
    
    ZVector<zint> vec2;
    TEST(vec2.isEmpty());
    
    vec2 = vec1;
    
    TEST(vec1.size()==3);
    TEST(vec2.getSize()==3);
  }
  
  //Тесты оператора присваивания перемещением для std::vector
  #if Z_STANDARD_MORE_11
  {
    std::vector<zint> vec1 { 10, 20, 30 };
    ZVector<zint> vec2;
    TEST(vec2.isEmpty());
    
    vec2 = std::move(vec1);
    
    TEST(vec1.empty());
    TEST(vec2.getSize()==3);
  }
  #endif
  
  //Тесты метода insert
  {
    ZVector<zint> vec;
    vec << 1 << 2 << 5 << 6;
    
    ZVector<zint>::Iterator i  ( vec.begin() + 2 );
    ZVector<zint>::Iterator i2 ( vec.insert(i, 3));
    TEST(*i2==3);
    TEST(vec.at(2)==3);
   
    ZVector<zint>::ConstIterator ci  ( vec.cbegin() + 3 );
    ZVector<zint>::ConstIterator ci2 ( vec.insert(ci, 4)); 
    TEST(*ci2==4);
    TEST(vec.at(3)==4);
    
    TEST(vec.at(4)==5);
    
  #if Z_STANDARD_MORE_11
    int r = 6;
    i = vec.insert(vec.end(), std::move(r));
    TEST(vec.at(5)==6);
  #endif
  }
#if Z_STANDARD_MORE_11
{
  ZVector<zint> vec{1, 5, 6};
  TEST(vec.at(1)==5);
  TEST(vec.getSize()==3);
  
  ZVector<zint>::Iterator i  { vec.begin() + 1 };
  ZVector<zint>::Iterator i2 { vec.insert(i, {2, 3, 4}) };
  TEST(vec.getSize()==6);
  
  TEST(vec.at(1)==2);
  TEST(vec.at(2)==3);
  TEST(vec.at(3)==4);
  TEST(vec.at(4)==5); 
}
#endif

  //Тесты методы erase
  {
    ZVector<zint> vec;
    vec << 1 << 2 << 3 << 4 << 5;
    TEST(vec.getSize()==5);
    
    ZVector<zint>::Iterator i ( vec.begin() + 1 );
    TEST(*i==2);
    i = vec.erase(i);
    TEST(vec.getSize()==4);
    TEST(*i==3);
    TEST(vec.at(1)==3);
    
    ZVector<zint>::ConstIterator ci (vec.cbegin() + 2 );
    TEST(*ci==4);
    i = vec.erase(ci);
    TEST(vec.getSize()==3);
    TEST(*i==5);
    TEST(vec.at(0)==1);
    TEST(vec.at(1)==3);
    TEST(vec.at(2)==5);
  }
  
  {    
    ZVector<zint> vec;
    vec << 1 << 2 << 2 << 2 << 3 << 4 << 4 << 4 << 5;
    TEST(vec.getSize() == 9);
    
    ZVector<zint>::Iterator ibegin ( vec.begin() + 1 );
    TEST(*ibegin==2);
    ZVector<zint>::Iterator iend   ( vec.begin() + 4 );
    TEST(*iend==3);
    ZVector<zint>::Iterator i( vec.erase(ibegin, iend) );
    TEST(vec.getSize()==6);
    TEST(*i==3);
    
    ZVector<zint>::ConstIterator cibegin ( vec.begin() + 2 );
    TEST(*cibegin==4);
    ZVector<zint>::ConstIterator ciend ( vec.begin() + 5 );
    TEST(*ciend==5);    
    i = vec.erase(cibegin, ciend );
    TEST(vec.getSize()==3);
    TEST(*i==5);
    
    i = vec.erase( vec.begin(), vec.end() );
    TEST(vec.isEmpty());    
  }
  
  //Тесты метода swap
  {
    ZVector<zint> vec1;
    vec1 << 1 << 3 << 5;
    TEST(vec1.at(0)==1);
    TEST(vec1.at(1)==3);
    TEST(vec1.at(2)==5);
    
    std::vector<zint> vec2;
    vec2.push_back(2);
    vec2.push_back(4);
    vec2.push_back(6);
    TEST(vec2.at(0)==2);
    TEST(vec2.at(1)==4);
    TEST(vec2.at(2)==6);
    
    vec1.swap(vec2);
    TEST(vec1.at(0)==2);
    TEST(vec1.at(1)==4);
    TEST(vec1.at(2)==6);
    TEST(vec2.at(0)==1);
    TEST(vec2.at(1)==3);
    TEST(vec2.at(2)==5);
  }
  
  {
    ZVector<zint> vec1;
    vec1 << 1 << 3 << 5;
    TEST(vec1.getSize()==3);
    TEST(vec1.at(0)==1);
    TEST(vec1.at(1)==3);
    TEST(vec1.at(2)==5);
    
    vec1.swap(vec1);
    TEST(vec1.getSize()==3);
    TEST(vec1.at(0)==1);
    TEST(vec1.at(1)==3);
    TEST(vec1.at(2)==5);
    
    ZVector<zint> vec2;
    vec2 << 2 << 4 << 6;
    TEST(vec2.getSize()==3);
    TEST(vec2.at(0)==2);
    TEST(vec2.at(1)==4);
    TEST(vec2.at(2)==6);
    
    vec1.swap(vec2);
    
    TEST(vec1.getSize()==3);
    TEST(vec1.at(0)==2);
    TEST(vec1.at(1)==4);
    TEST(vec1.at(2)==6);
    
    TEST(vec2.getSize()==3);
    TEST(vec2.at(0)==1);
    TEST(vec2.at(1)==3);
    TEST(vec2.at(2)==5);
  }

  //Тесты метода shrink
  {
    ZVector<zint> numbers(20);
    numbers << 1 << 2 << 3 << 4 << 5;
    TEST(numbers.getSize()==5);
    TEST(numbers.getCapacity()==20);
    
    numbers.shrink();
    TEST(numbers.getSize()==5);
    TEST(numbers.getCapacity()==5);
    TEST(numbers[0]==1);
    TEST(numbers[1]==2);
    TEST(numbers[2]==3);
    TEST(numbers[3]==4);
    TEST(numbers[4]==5);
    
    ZVector<zint> numbers2(20);
    TEST(numbers2.isEmpty());
    TEST(numbers2.getCapacity()==20);
    
    numbers2.shrink();
    TEST(numbers2.isEmpty());
    TEST(numbers2.getCapacity()==0);    
  }
  
  //Тесты метода getData
  {
    ZVector<zint> numbers;
    TEST(numbers.isEmpty());
    TEST(numbers.getData()==Z_NULL_PTR);
    
    numbers << 1 << 2 << 3;
    zint *p = numbers.getData();
    TEST(*p==1);
    *p = 25;
    TEST(numbers.at(0)==25);
    TEST(p[1]==2);
    TEST(p[2]==3);    
  }

  //Тесты метода emplace
  {
    ZVector<zint> numbers;
    numbers << 1 << 4;
    TEST(numbers.getSize()==2);
    ZVector<zint>::Iterator i (numbers.end() - 1);
    TEST(*i==4);
    
    i = numbers.emplace(i);
    TEST(numbers.getSize()==3);
    
    ZVector<zint>::ConstIterator ci (numbers.cbegin() + 1);
    
    ci = numbers.emplace(ci);
    TEST(numbers.getSize()==4);
  }
  
  {
    ZVector<zint> numbers;
    numbers << 1 << 4;
    TEST(numbers.getSize()==2);
    ZVector<zint>::Iterator i (numbers.end() - 1);
    TEST(*i==4);
    
    i = numbers.emplace(i, 3);
    TEST(numbers.getSize()==3);
    TEST(*i==3);
    
    ZVector<zint>::ConstIterator ci (numbers.cbegin() + 1);
    
    ci = numbers.emplace(ci, 2);
    TEST(numbers.getSize()==4);
    TEST(*ci==2);
  }
  
  {
    ZVector<Two> twos;
    twos << Two(3,6);
    TEST(twos.getSize()==1);
    
    ZVector<Two>::Iterator i (twos.begin());
    TEST(*i==Two(3,6));
    
    i = twos.emplace(i, 2, 4);
    TEST(twos.getSize()==2);
    TEST(*i==Two(2,4));
    
    ZVector<Two>::ConstIterator ci (twos.cbegin());
    TEST(*ci==Two(2,4));
    
    ci = twos.emplace(i, 1, 2);
    TEST(twos.getSize()==3);
    TEST(*ci==Two(1,2));
  }
  
  {
    ZVector<Three> threes;
    threes << Three(3,6,9);
    TEST(threes.getSize()==1);
    
    ZVector<Three>::Iterator i (threes.begin());
    TEST(*i==Three(3,6,9));
    
    i = threes.emplace(i, 2, 4, 6);
    TEST(threes.getSize()==2);
    TEST(*i==Three(2,4,6));
    
    ZVector<Three>::ConstIterator ci (threes.cbegin());
    TEST(*ci==Three(2,4,6));
    
    ci = threes.emplace(ci, 1, 2, 3);
    TEST(threes.getSize()==3);
    TEST(*ci==Three(1,2,3));
  }
  
  //Тесты совместимости с STL
  {
    ZVector<zint> numbers;
    numbers << 5 << 4 << 3;
    std::sort(numbers.begin(), numbers.end());
    TEST(numbers.at(0)==3);
    TEST(numbers.at(1)==4);
    TEST(numbers.at(2)==5);
    
    std::sort(numbers.rbegin(), numbers.rend());
    TEST(numbers.at(0)==5);
    TEST(numbers.at(1)==4);
    TEST(numbers.at(2)==3);
    
    ZVector<zint>::ConstIterator i;
    i = std::find(numbers.cbegin(), numbers.cend(), 5);
    TEST(*i==5);
    
    ZVector<zint>::ConstReverseIterator j;
    j = std::find(numbers.crbegin(), numbers.crend(), 3);
    TEST(*j==3);
  }

  return 0;
}
