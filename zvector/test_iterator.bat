@echo off
setlocal enabledelayedexpansion

set in_file=%1%.cpp
set out_file=%1%.exe

g++ %in_file% -o %out_file% -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

call %out_file%
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

del %out_file%
rem Ловим ассерты непустого итератора
for /L %%j in (0,1,22) do (    
  if exist %out_file% del %out_file%
  
  g++ %in_file% -o %out_file% -DI_ASSERT=%%j -Wall -Wextra -pedantic %inc_dir%
  if ERRORLEVEL 1 (
    echo "test zvector failed (I_ASSERT=%%j)"
    exit /b 1
  )
  
  start /b /wait %out_file% >nul 2>&1  
  
  if not !ERRORLEVEL!==3 (
    echo "test zvector failed"
    exit /b 1
  )
  del %out_file%  
)
rem Пустой итератор
for /L %%j in (0,1,15) do (
  if exist %out_file% del %out_file%
  
  g++ %in_file% -o %out_file% -DEMPTY= -DI_ASSERT=%%j -Wall -Wextra -pedantic %inc_dir%
  if ERRORLEVEL 1 (
    echo "test zvector failed (EMPTY, I_ASSERT=%%j)"
    exit /b 1
  )
  
  start /b /wait %out_file% >nul 2>&1  
  
  if not !ERRORLEVEL!==3 (
    echo "test zvector failed"
    exit /b 1
  )
  del %out_file%  
)

exit /b 0