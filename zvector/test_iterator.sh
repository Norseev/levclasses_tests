#!/bin/sh

#1 - compiler name
#2 - file name without extension
#3 - extension
#4 - aditional compiler options
#5 - wine, if present
#6 - error code
test_once()
{
  rm -f $2$3
  
  $1 $2.cpp -o $2$3 $4 -Wall -Wextra -pedantic $inc_dir
  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi

  $5 ./$2$3
  if [ $? -ne 0 ]; then
    echo "test zvector failed"
    exit 1
  fi
  
  rm -f $2$3
  
  for i in $(seq 0 1 22); do
    $1 $2.cpp -o $2$3 $4 -DI_ASSERT=$i -Wall -Wextra -pedantic $inc_dir
    if [ $? -ne 0 ]; then
      echo "test zvector failed ($2, ASSERT=$i)"
      exit 1
    fi
  
    $5 ./$2$3 >/dev/null 2>&1
    if [ $? -ne $6 ]; then
      echo "test zvector failed  ($2, ASSERT=$i)"
      echo $i      
      exit 1
    fi
  
    rm -f $2$3
  done
    
  #Тестируем пустой итератор
  for i in $(seq 0 1 15); do
    $1 $2.cpp -o $2$3 $4 -DEMPTY= -DI_ASSERT=$i -Wall -Wextra -pedantic $inc_dir
    if [ $? -ne 0 ]; then
      echo "test zvector failed ($2, EMPTY, ASSERT=$i)"
      exit 1
    fi
  
    $5 ./$2$3 >/dev/null 2>&1
    if [ $? -ne $6 ]; then
      echo "test zvector failed ($2, EMPTY, ASSERT=$i)"
      exit 1
    fi
  
    rm -f $2$3    
 done
 
 return 0  
}

#Linux 64 bit
test_once "g++" "$1" "" "" "" "134"
if [ $? -ne 0 ]; then
  exit 1
fi

#Linux 32 bit
test_once "g++" "$1" "" "-m32" "" "134"
if [ $? -ne 0 ]; then
  exit 1
fi

#Windows 64 bit
test_once "x86_64-w64-mingw32-g++" "$1" ".exe" "-static-libgcc -static-libstdc++" "wine" "3"
if [ $? -ne 0 ]; then
  exit 1
fi

#Windows 32 bit
test_once "i686-w64-mingw32-g++" "$1" ".exe" "-static-libgcc -static-libstdc++" "wine" "3"
if [ $? -ne 0 ]; then
  exit 1
fi

exit 0
