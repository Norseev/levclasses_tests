@echo off

echo "start testing zvector (no tests)"

exit /b 0

rem Тесты для стандарта С++98
g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 -DZ_USE_FAST_ITERATORS %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

del main.exe

rem Тесты для стандарта С++11
g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 -DZ_USE_FAST_ITERATORS %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zvector failed"
  exit /b 1
)

del main.exe

rem Тестируем итераторы
call test_iterator iterator
if ERRORLEVEL 1  exit /b 1

call test_iterator const_iterator
if ERRORLEVEL 1  exit /b 1

g++ const_iterator.cpp -o const_iterator.exe -DTRY_CHANGE= -Wall -Wextra -pedantic %inc_dir% >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zvector failed"
  exit /b 1
)

g++ const_iterator.cpp -o const_iterator.exe -DTRY_CHANGE2= -Wall -Wextra -pedantic %inc_dir% >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zvector failed"
  exit /b 1
)

call test_iterator reverse_iterator
if ERRORLEVEL 1  exit /b 1

call test_iterator const_reverse_iterator
if ERRORLEVEL 1  exit /b 1

g++ const_reverse_iterator.cpp -o const_reverse_iterator.exe -DTRY_CHANGE= -Wall -Wextra -pedantic %inc_dir% >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zvector failed"
  exit /b 1
)

g++ const_reverse_iterator.cpp -o const_reverse_iterator.exe -DTRY_CHANGE2= -Wall -Wextra -pedantic %inc_dir% >nul 2>&1
if %ERRORLEVEL%==0 (
  echo "test zvector failed"
  exit /b 1
)

exit /b 0
