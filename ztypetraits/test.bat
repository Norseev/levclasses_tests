@echo off

echo "start testing ztypetraits"

if exist main.exe del main.exe

g++ main.cpp -o main.exe -std=c++98 -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test ztypetraits failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test ztypetraits failed"
  exit /b 1
)
del main.exe

g++ main.cpp -o main.exe -std=c++98 -Wall -Wextra -pedantic -DTEST_ENABLE_IF= %inc_dir% >null 2>&1
if %ERRORLEVEL%==0 (
  echo "test ztypetraits failed"
  exit /b 1
)

g++ main.cpp -o main.exe -std=c++11 -Wall -Wextra -pedantic %inc_dir% 
if ERRORLEVEL 1 (
  echo "test ztypetraits failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test ztypetraits failed"
  exit /b 1
)
del main.exe

g++ main.cpp -o main.exe -std=c++11 -Wall -Wextra -pedantic -DTEST_ENABLE_IF= %inc_dir% >null 2>&1
if %ERRORLEVEL%==0 (
  echo "test ztypetraits failed"
  exit /b 1
)

exit /b 0
