
#include <ztypetraits>
#include <zunicodecharacter>
#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

bool testIsSameType() Z_NOEXCEPT
{
  TEST((ZIsSameType<int,int>::value));
  TEST((! ZIsSameType<int,const int>::value));
  TEST((! ZIsSameType<int,volatile int>::value));
  TEST((! ZIsSameType<int,int*>::value));
  TEST((! ZIsSameType<int,int&>::value));
#ifdef Z_MOVE_SEMANTICS
  TEST((! ZIsSameType<int,int&&>::value));  
#endif  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveConst() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveConst<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveConst<const int>::type>::value));
  TEST((ZIsSameType<const int*, ZRemoveConst<const int*>::type>::value));
  TEST((ZIsSameType<int*, ZRemoveConst<int* const>::type>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveVolatile() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveVolatile<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveVolatile<volatile int>::type>::value));
  TEST((ZIsSameType<volatile int*, ZRemoveVolatile<volatile int*>::type>::value));
  TEST((ZIsSameType<int*, ZRemoveVolatile<int* volatile>::type>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveCV() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveCV<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveCV<const int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveCV<volatile int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveCV<const volatile int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveCV<volatile const int>::type>::value));
  
  TEST((ZIsSameType<const int*, ZRemoveCV<const int*>::type>::value));
  TEST((ZIsSameType<volatile int*, ZRemoveCV<volatile int*>::type>::value));
  TEST((ZIsSameType<const volatile int*, ZRemoveCV<const volatile int*>::type>::value));
  
  TEST((ZIsSameType<int*, ZRemoveCV<int*>::type>::value));
  TEST((ZIsSameType<int*, ZRemoveCV<int* const>::type>::value));
  TEST((ZIsSameType<int*, ZRemoveCV<int* volatile>::type>::value));
  TEST((ZIsSameType<int*, ZRemoveCV<int* const volatile>::type>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveExtent() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveExtent<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveExtent<int[]>::type>::value));
  TEST((ZIsSameType<int, ZRemoveExtent<int[5]>::type>::value));
  
  TEST((ZIsSameType<int[2], ZRemoveExtent<int[1][2]>::type>::value));
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveAllExtents() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveAllExtents<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveAllExtents<int[]>::type>::value));
  TEST((ZIsSameType<int, ZRemoveAllExtents<int[5]>::type>::value));
  TEST((ZIsSameType<int, ZRemoveAllExtents<int[1][2]>::type>::value));
  TEST((ZIsSameType<int, ZRemoveAllExtents<int[1][2][3][4]>::type>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemoveReference() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemoveReference<int>::type>::value));
  TEST((ZIsSameType<int, ZRemoveReference<int&>::type>::value));
#ifdef Z_MOVE_SEMANTICS
  TEST((ZIsSameType<int, ZRemoveReference<int&&>::type>::value));
#endif  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testRemovePointer() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZRemovePointer<int>::type>::value));
  TEST((ZIsSameType<int, ZRemovePointer<int*>::type>::value));
  
  TEST((ZIsSameType<const int, ZRemovePointer<const int*>::type>::value));
  TEST((ZIsSameType<volatile int, ZRemovePointer<volatile int*>::type>::value));
  TEST((ZIsSameType<const volatile int, ZRemovePointer<const volatile int*>::type>::value));
  
  TEST((ZIsSameType<int, ZRemovePointer<int* const>::type>::value));
  TEST((ZIsSameType<int, ZRemovePointer<int* volatile>::type>::value));
  TEST((ZIsSameType<int, ZRemovePointer<int* const volatile >::type>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
void testEnableIf() Z_NOEXCEPT
{
  ZEnableIf<true,int>::type a = 0;
  ((void)a);
#ifdef TEST_ENABLE_IF
  ZEnableIf<false,int>::type b = 0;
#endif  
}
////////////////////////////////////////////////////////////////////////////////
bool testConditional() Z_NOEXCEPT
{
  TEST((ZIsSameType<int , ZConditional<true , int, char>::type>::value));
  TEST((ZIsSameType<char, ZConditional<false, int, char>::type>::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsAnsiCharacter() Z_NOEXCEPT
{
  TEST((! ZIsAnsiCharacter<int>::value));
  
  TEST((ZIsAnsiCharacter<char>::value));
  TEST((ZIsAnsiCharacter<const char>::value));
  TEST((ZIsAnsiCharacter<volatile char>::value));
  TEST((ZIsAnsiCharacter<const volatile char>::value));
  
  TEST((ZIsAnsiCharacter<zchar>::value));
  TEST((ZIsAnsiCharacter<const zchar>::value));
  TEST((ZIsAnsiCharacter<volatile zchar>::value));
  TEST((ZIsAnsiCharacter<const volatile zchar>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsArray() Z_NOEXCEPT
{
  TEST((! ZIsArray<int>::value));
  TEST((! ZIsArray<int*>::value));
  TEST((ZIsArray<int[]>::value));
  TEST((ZIsArray<int[5]>::value));
  TEST((ZIsArray<int[1][2]>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsBool() Z_NOEXCEPT
{
  TEST((!ZIsBool<int>::value));
  
  TEST((ZIsBool<bool>::value));
  TEST((ZIsBool<const bool>::value));
  TEST((ZIsBool<volatile bool>::value));
  TEST((ZIsBool<const volatile bool>::value));
  
  TEST((ZIsBool<zbool>::value));
  TEST((ZIsBool<const zbool>::value));
  TEST((ZIsBool<volatile zbool>::value));
  TEST((ZIsBool<const volatile zbool>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsCharacter() Z_NOEXCEPT
{
  TEST((! ZIsCharacter<bool>::value));
  TEST((! ZIsCharacter<float>::value));
  
  TEST((ZIsCharacter<char>::value));
  TEST((ZIsCharacter<wchar_t>::value));
  TEST((ZIsCharacter<zchar>::value));
  TEST((ZIsCharacter<zchar8>::value));
  TEST((ZIsCharacter<zchar16>::value));
  TEST((ZIsCharacter<zchar32>::value));
  TEST((ZIsCharacter<zsingleunichar>::value));
  TEST((ZIsCharacter<zunichar>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsConst() Z_NOEXCEPT
{
  TEST((! ZIsConst<int>::value));
  TEST((ZIsConst<const int>::value));
  TEST((ZIsConst<const volatile int>::value));
  TEST((ZIsConst<volatile const int>::value));
  
  TEST((! ZIsConst<const int*>::value));
  TEST((! ZIsConst<const volatile int*>::value));
  
  TEST((ZIsConst<int* const>::value));
  TEST((ZIsConst<int* const volatile>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsLValueReference() Z_NOEXCEPT
{
  TEST((!ZIsLValueReference<int>::value));
  TEST((ZIsLValueReference<int&>::value));
  TEST((ZIsLValueReference<const int&>::value));
#ifdef Z_MOVE_SEMANTICS
  TEST((!ZIsLValueReference<int&&>::value));
#endif
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsRValueReference() Z_NOEXCEPT
{
  TEST((!ZIsRValueReference<int>::value));
  TEST((!ZIsRValueReference<int&>::value));
  TEST((!ZIsRValueReference<const int&>::value));
#ifdef Z_MOVE_SEMANTICS
  TEST((ZIsRValueReference<int&&>::value));
#endif
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsReference() Z_NOEXCEPT
{
  TEST((!ZIsReference<int>::value));
  TEST((ZIsReference<int&>::value));
  TEST((ZIsReference<const int&>::value));
#ifdef Z_MOVE_SEMANTICS
  TEST((ZIsReference<int&&>::value));
#endif
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsSignedInteger() Z_NOEXCEPT
{
  TEST((! ZIsSignedInteger<bool>::value));
  TEST((! ZIsSignedInteger<float>::value));
  TEST((! ZIsSignedInteger<double>::value));
  
  TEST((! ZIsSignedInteger<unsigned char>::value));
  TEST((! ZIsSignedInteger<unsigned short>::value));
  TEST((! ZIsSignedInteger<unsigned int>::value));
  TEST((! ZIsSignedInteger<unsigned long>::value));
#if Z_COMPILER_GNU && Z_STANDARD_MORE_11
  TEST((! ZIsSignedInteger<unsigned long long>::value));
#endif
  TEST((! ZIsSignedInteger<size_t>  ::value));

  TEST((! ZIsSignedInteger<zuint8>::value));
  TEST((! ZIsSignedInteger<zuint16>::value));
  TEST((! ZIsSignedInteger<zuint32>::value));
  TEST((! ZIsSignedInteger<zuint64>::value));
  TEST((! ZIsSignedInteger<zuint>  ::value));
  
  TEST((! ZIsSignedInteger<const zuint>  ::value));
  TEST((! ZIsSignedInteger<volatile zuint>  ::value));
  TEST((! ZIsSignedInteger<const volatile zuint>  ::value));  
  
  TEST((ZIsSignedInteger<signed char>::value));
  TEST((ZIsSignedInteger<signed short>::value));
  TEST((ZIsSignedInteger<signed int>::value));
  TEST((ZIsSignedInteger<signed long>::value));
  
  TEST((ZIsSignedInteger<short>::value));
  TEST((ZIsSignedInteger<int>::value));
  TEST((ZIsSignedInteger<long>::value));
  
#if Z_COMPILER_GNU && Z_STANDARD_MORE_11
  TEST((ZIsSignedInteger<signed long long>::value));
  TEST((ZIsSignedInteger<long long>::value));
#endif

  TEST((ZIsSignedInteger<zint8> ::value));
  TEST((ZIsSignedInteger<zint16>::value));
  TEST((ZIsSignedInteger<zint32>::value));
  TEST((ZIsSignedInteger<zint64>::value));
  TEST((ZIsSignedInteger<zint>  ::value));
  
  TEST((ZIsSignedInteger<const zint>::value));
  TEST((ZIsSignedInteger<volatile zint>::value));
  TEST((ZIsSignedInteger<const volatile zint>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsUnsignedInteger() Z_NOEXCEPT
{
  TEST((! ZIsUnsignedInteger<bool>::value));
  TEST((! ZIsUnsignedInteger<float>::value));
  TEST((! ZIsUnsignedInteger<double>::value));
  
  TEST((! ZIsUnsignedInteger<signed char>::value));
  TEST((! ZIsUnsignedInteger<signed short>::value));
  TEST((! ZIsUnsignedInteger<signed int>::value));
  TEST((! ZIsUnsignedInteger<signed long>::value));
  
  TEST((! ZIsUnsignedInteger<short>::value));
  TEST((! ZIsUnsignedInteger<int>::value));
  TEST((! ZIsUnsignedInteger<long>::value));
  
#if Z_COMPILER_GNU && Z_STANDARD_MORE_11
  TEST((! ZIsUnsignedInteger<signed long long>::value));
  TEST((! ZIsUnsignedInteger<long long>::value));
#endif

  TEST((! ZIsUnsignedInteger<zint8> ::value));
  TEST((! ZIsUnsignedInteger<zint16>::value));
  TEST((! ZIsUnsignedInteger<zint32>::value));
  TEST((! ZIsUnsignedInteger<zint64>::value));
  TEST((! ZIsUnsignedInteger<zint>  ::value));
  
  TEST((! ZIsUnsignedInteger<const zint>::value));
  TEST((! ZIsUnsignedInteger<volatile zint>::value));
  TEST((! ZIsUnsignedInteger<const volatile zint>::value));
  
  TEST((ZIsUnsignedInteger<unsigned char>::value));
  TEST((ZIsUnsignedInteger<unsigned short>::value));
  TEST((ZIsUnsignedInteger<unsigned int>::value));
  TEST((ZIsUnsignedInteger<unsigned long>::value));
#if Z_COMPILER_GNU && Z_STANDARD_MORE_11
  TEST((ZIsUnsignedInteger<unsigned long long>::value));
#endif
  TEST((ZIsUnsignedInteger<size_t>::value));

  TEST((ZIsUnsignedInteger<zuint8>::value));
  TEST((ZIsUnsignedInteger<zuint16>::value));
  TEST((ZIsUnsignedInteger<zuint32>::value));
  TEST((ZIsUnsignedInteger<zuint64>::value));
  TEST((ZIsUnsignedInteger<zuint>  ::value));
  
  TEST((ZIsUnsignedInteger<const zuint>  ::value));
  TEST((ZIsUnsignedInteger<volatile zuint>  ::value));
  TEST((ZIsUnsignedInteger<const volatile zuint>  ::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsInteger() Z_NOEXCEPT
{
  TEST((! ZIsInteger<bool>::value));
  TEST((! ZIsInteger<float>::value));
  TEST((! ZIsInteger<double>::value));
  
  TEST((ZIsInteger<char>::value));
  TEST((ZIsInteger<signed char>::value));
  TEST((ZIsInteger<unsigned char>::value));
  
  TEST((ZIsInteger<int>::value));
  TEST((ZIsInteger<short>::value));
  TEST((ZIsInteger<long>::value));
  TEST((ZIsInteger<size_t>::value));
  
  TEST((ZIsInteger<const int>::value));
  TEST((ZIsInteger<volatile int>::value));
  TEST((ZIsInteger<const volatile int>::value));  
  
  TEST((ZIsInteger<zint8>::value));
  TEST((ZIsInteger<zint16>::value));
  TEST((ZIsInteger<zint32>::value));
  TEST((ZIsInteger<zint64>::value));
  TEST((ZIsInteger<zint>::value));
  
  TEST((ZIsInteger<zuint8>::value));
  TEST((ZIsInteger<zuint16>::value));
  TEST((ZIsInteger<zuint32>::value));
  TEST((ZIsInteger<zuint64>::value));
  TEST((ZIsInteger<zuint>::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsFloatingPoint() Z_NOEXCEPT
{
  TEST((! ZIsFloatingPoint<bool>::value));
  TEST((! ZIsFloatingPoint<int>::value));
  TEST((! ZIsFloatingPoint<unsigned int>::value));
  
  TEST((ZIsFloatingPoint<float>::value));
  TEST((ZIsFloatingPoint<const float>::value));
  TEST((ZIsFloatingPoint<volatile float>::value));
  TEST((ZIsFloatingPoint<const volatile float>::value));
  
  TEST((ZIsFloatingPoint<double>::value));
  TEST((ZIsFloatingPoint<const double>::value));
  TEST((ZIsFloatingPoint<volatile double>::value));
  TEST((ZIsFloatingPoint<const volatile double>::value));
  
  TEST((ZIsFloatingPoint<long double>::value));
  
  TEST((ZIsFloatingPoint<zreal>::value));
  TEST((ZIsFloatingPoint<const zreal>::value));
  TEST((ZIsFloatingPoint<volatile zreal>::value));
  TEST((ZIsFloatingPoint<const volatile zreal>::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsArithmetic() Z_NOEXCEPT
{
  TEST((! ZIsArithmetic<bool>::value));
  TEST((! ZIsArithmetic<int*>::value));
  TEST((! ZIsArithmetic<int&>::value));
  TEST((! ZIsArithmetic<float*>::value));
  TEST((! ZIsArithmetic<float&>::value));
  
  TEST((ZIsArithmetic<char>::value));
  TEST((ZIsArithmetic<short>::value));
  TEST((ZIsArithmetic<int>::value));
  TEST((ZIsArithmetic<long>::value));
  TEST((ZIsArithmetic<size_t>::value));
  
  TEST((ZIsArithmetic<const int>::value));
  TEST((ZIsArithmetic<volatile int>::value));
  TEST((ZIsArithmetic<const volatile int>::value));
  
  TEST((ZIsArithmetic<zint8>::value));
  TEST((ZIsArithmetic<zint16>::value));
  TEST((ZIsArithmetic<zint32>::value));
  TEST((ZIsArithmetic<zint64>::value));
  TEST((ZIsArithmetic<zint>::value));
  
  TEST((ZIsArithmetic<zuint8>::value));
  TEST((ZIsArithmetic<zuint16>::value));
  TEST((ZIsArithmetic<zuint32>::value));
  TEST((ZIsArithmetic<zuint64>::value));
  TEST((ZIsArithmetic<zuint>::value));
  
  TEST((ZIsArithmetic<float>::value));
  TEST((ZIsArithmetic<double>::value));
  TEST((ZIsArithmetic<long double>::value));
  TEST((ZIsArithmetic<zreal>::value));
  
  TEST((ZIsArithmetic<const float>::value));
  TEST((ZIsArithmetic<volatile float>::value));
  TEST((ZIsArithmetic<const volatile float>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsVolatile() Z_NOEXCEPT
{
  TEST((! ZIsVolatile<int>::value));
  TEST((ZIsVolatile<volatile int>::value));
  TEST((ZIsVolatile<const volatile int>::value));
  TEST((ZIsVolatile<volatile const int>::value));
  
  TEST((! ZIsVolatile<volatile int*>::value));
  TEST((! ZIsVolatile<const volatile int*>::value));
  
  TEST((ZIsVolatile<int* volatile>::value));
  TEST((ZIsVolatile<int* const volatile>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
class TestIsSimpleType {}; 

bool testIsSimpleType() Z_NOEXCEPT
{
  TEST((! ZIsSimpleType<int*>::value));
  TEST((! ZIsSimpleType<int&>::value));
  TEST((! ZIsSimpleType<int[5]>::value));
  TEST((! ZIsSimpleType<TestIsSimpleType>::value));
  
  TEST((ZIsSimpleType<char>::value));
  TEST((ZIsSimpleType<short>::value));
  TEST((ZIsSimpleType<int>::value));
  TEST((ZIsSimpleType<long>::value));
  TEST((ZIsSimpleType<size_t>::value));
  
  TEST((ZIsSimpleType<zint8>::value));
  TEST((ZIsSimpleType<zint16>::value));
  TEST((ZIsSimpleType<zint32>::value));
  TEST((ZIsSimpleType<zint64>::value));
  TEST((ZIsSimpleType<zint>::value));
  
  TEST((ZIsSimpleType<zuint8>::value));
  TEST((ZIsSimpleType<zuint16>::value));
  TEST((ZIsSimpleType<zuint32>::value));
  TEST((ZIsSimpleType<zuint64>::value));
  TEST((ZIsSimpleType<zuint>::value));
  
  TEST((ZIsSimpleType<float>::value));
  TEST((ZIsSimpleType<double>::value));
  TEST((ZIsSimpleType<long double>::value));
  TEST((ZIsSimpleType<zreal>::value));
  
  TEST((ZIsSimpleType<wchar_t>::value));
  TEST((ZIsSimpleType<zchar>::value));  
  TEST((ZIsSimpleType<zchar16>::value));
  TEST((ZIsSimpleType<zchar32>::value));
  
  TEST((ZIsSimpleType<bool>::value));
  TEST((ZIsSimpleType<zbool>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsPointer() Z_NOEXCEPT
{
  TEST((!ZIsPointer<int>::value));
  TEST((ZIsPointer<int*>::value));
  
  TEST((ZIsPointer<const int*>::value));
  TEST((ZIsPointer<volatile int*>::value));
  TEST((ZIsPointer<const volatile int*>::value));
  
  TEST((ZIsPointer<int* const>::value));
  TEST((ZIsPointer<int* volatile>::value));
  TEST((ZIsPointer<int* const volatile>::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testIsUnicodeCharacter() Z_NOEXCEPT
{
  TEST((ZIsUnicodeCharacter<zchar16>::value));
  TEST((ZIsUnicodeCharacter<const zchar16>::value));
  TEST((ZIsUnicodeCharacter<volatile zchar16>::value));
  TEST((ZIsUnicodeCharacter<const volatile zchar16>::value));
  
  TEST((ZIsUnicodeCharacter<zchar32>::value));
  TEST((ZIsUnicodeCharacter<const zchar32>::value));
  TEST((ZIsUnicodeCharacter<volatile zchar32>::value));
  TEST((ZIsUnicodeCharacter<const volatile zchar32>::value));
  
  TEST((ZIsUnicodeCharacter<zsingleunichar>::value));
  TEST((ZIsUnicodeCharacter<const zsingleunichar>::value));
  TEST((ZIsUnicodeCharacter<volatile zsingleunichar>::value));
  TEST((ZIsUnicodeCharacter<const volatile zsingleunichar>::value));
  
  TEST((ZIsUnicodeCharacter<wchar_t>::value));
  TEST((ZIsUnicodeCharacter<const wchar_t>::value));
  TEST((ZIsUnicodeCharacter<volatile wchar_t>::value));
  TEST((ZIsUnicodeCharacter<const volatile wchar_t>::value));
    
  TEST((!ZIsUnicodeCharacter<char>::value));  
  TEST((!ZIsUnicodeCharacter<bool>::value));
  TEST((!ZIsUnicodeCharacter<zreal>::value));
  
  //Undefined behavior:
  //TEST((!ZIsUnicodeCharacter<int>::value));
  //TEST((!ZIsUnicodeCharacter<short>::value));
  //TEST((ZIsUnicodeCharacter<long>::value));
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool testDecayType() Z_NOEXCEPT
{
  TEST((ZIsSameType<int, ZDecayType<int>::type>::value));
  TEST((ZIsSameType<int, ZDecayType<const int>::type>::value));
  TEST((ZIsSameType<int, ZDecayType<volatile int>::type>::value));
 
  TEST((ZIsSameType<int, ZDecayType<int&>::type>::value));  
#ifdef Z_MOVE_SEMANTICS  
  TEST((ZIsSameType<int, ZDecayType<int&&>::type>::value));
#endif 
  TEST((ZIsSameType<int, ZDecayType<const int&>::type>::value));
 
  TEST((ZIsSameType<int*, ZDecayType<int[2]>::type>::value));  
  
  return true;
}
////////////////////////////////////////////////////////////////////////////////
bool test() Z_NOEXCEPT
{  
  return testIsSameType()       && testRemoveConst()    && testRemoveExtent()&&
         testRemoveAllExtents() && testRemoveVolatile() && testRemoveCV()    &&
         testRemoveReference()  && testRemovePointer()  && testIsArray()     &&
         testIsConst()          && testIsVolatile()     && testIsPointer()   &&
         testConditional()      && testDecayType() && testIsLValueReference()&&
         testIsRValueReference()&& testIsReference()    && testIsBool()      &&
         testIsAnsiCharacter()&& testIsUnicodeCharacter()&& testIsCharacter()&&
         testIsSignedInteger() && testIsUnsignedInteger() && testIsInteger() &&
         testIsFloatingPoint() && testIsArithmetic() && testIsSimpleType();
}
////////////////////////////////////////////////////////////////////////////////
int main()
{
  return test() ? 0 : 1;
}
