#!/bin/sh

echo "start testing ztypetraits"

# $1 - имя скомпилированного файла
# $2 - утилита wine, если производится компиляция под Windows
test_once()
{
  rm -f $1
  $compile_string

  if [ $? != 0 ]; then
    echo "test ztypetraits failed"
    exit 1
  fi

  $2 ./$1
  if [ $? != 0 ]; then
    echo "test ztypetraits failed"
    exit 1
  fi
  
  rm -f $1
  $compile_string -DTEST_ENABLE_IF=  >/dev/null 2>&1
  if [ $? = 0 ]; then
    echo "test ztypetraits failed"
    exit 1
  fi 
}
#Linux
compile_string="g++ main.cpp -o main -std=c++98 -Wall -Wextra -pedantic $inc_dir" 
test_once main
if [ $? != 0 ]; then
  exit 1
fi

compile_string="g++ main.cpp -o main -std=c++11 -Wall -Wextra -pedantic $inc_dir" 
test_once main  
if [ $? != 0 ]; then
  exit 1
fi

rm -f main

#Windows 32 bit
compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++98 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi

compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++11 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi
#Windows 64 bit
compile_string="x86_64-w64-mingw32-g++ main.cpp -o main.exe -std=c++98 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi

compile_string="x86_64-w64-mingw32-g++ main.cpp -o main.exe -std=c++11 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi

rm -f main.exe

exit 0
