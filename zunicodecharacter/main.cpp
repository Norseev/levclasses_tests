#include <ztypes>
#include <zunicodecharacter>

#include <string.h>
#include <wchar.h>
#include <cassert>

#include <wctype.h>

#include "../test.h"

using namespace Z_GLOBAL_NAMESPACE;

zuint getWLen(const zunichar *str)
{
  if(sizeof(zunichar)==sizeof(wchar_t))
    return wcslen(reinterpret_cast<const wchar_t*>(str));

  assert(sizeof(zunichar)==1);
  return strlen(reinterpret_cast<const char*>(str));
}

zbool isAlpha(zsingleunichar symbol)
{
  return (iswalpha(symbol) != 0);
}

zbool isDigit(zsingleunichar symbol)
{
  return (iswdigit(symbol) != 0);
}

bool test()
{
#if defined Z_OS_WINDOWS
  TEST(sizeof(zunichar)==sizeof(wchar_t));
  TEST(sizeof(zsingleunichar)==sizeof(wchar_t));
#elif defined Z_OS_LINUX
  TEST((sizeof(zunichar)==sizeof(zchar8) || sizeof(zunichar)==sizeof(wchar_t)));
  TEST(sizeof(zsingleunichar)==sizeof(wchar_t));
#else
  TEST(false);
#endif

  TEST(getWLen(Z_TEXT("qwerty"))==6);

  TEST(isAlpha(Z_CHAR('D')));
  TEST(!isAlpha(Z_CHAR('#')));
  TEST(isDigit(Z_CHAR('5')));
  TEST(!isDigit(Z_CHAR('D')));

  return true;
}

int main()
{
  return (test() ? 0 : 1);
}
