#!/bin/sh

export inc_dir=-I"../../lev/Include"

if [ -z "$1" ]; then
  for catalog in $(ls -d */); do
    cd $catalog
    ./test.sh
    if [ $? != 0 ]; then    
      exit 1 
    fi
    cd ../
  done
else
  cd $1 && ./test.sh
fi

exit 0
