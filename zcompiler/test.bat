@echo off

echo "start testing zcompiler"

if exist main.exe del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic %inc_dir%
if ERRORLEVEL 1 (
  echo "test zcompiler failed"
  exit /b 1
)

main.exe | find "MinGW" >nul
if ERRORLEVEL 1 (
  echo "test zcompiler failed"
  exit /b 1
)

main.exe >nul
if ERRORLEVEL 1 (
  echo "test zcompiler failed"
  exit /b 1
)

del main.exe

exit /b 0
