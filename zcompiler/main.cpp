

#include <compiler/zcompiler>

#include <cstdio>

int main()
{
  
  int a = Z_COMPILER_VERSION;
  ((void)a);
  
#if Z_COMPILER == Z_COMPILER_GCC

  printf("GCC\r\n");
  return Z_COMPILER_GNU ? 0 : 1;

#elif Z_COMPILER == Z_COMPILER_MINGW

  printf("MinGW\r\n");
  return Z_COMPILER_GNU ? 0 : 1;

#elif Z_COMPILER == Z_COMPILER_UNKNOWN

  printf("unknown\r\n");
  return !Z_COMPILER_GNU ? 0 : 1;

#else
  #error Unknown value of Z_COMPILER
#endif
  
}
