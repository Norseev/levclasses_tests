#!/bin/sh

echo "start testing zcompiler"

# $1 - имя скомпилированного файла
# $2 - ожидаемая строка
# $3 - wine, если есть
test_once () 
{
  rm -f $1
  
  $compile_string  
  if [ $? != 0 ]; then
    echo "test zcompiler failed"
    exit 1
  fi
  
  $3 ./$1 | grep $2 > /dev/null
  if [ $? != 0 ]; then
    echo "test zcompiler failed"
    exit 1
  fi
  
  $3 ./$1 > /dev/null
  if [ $? != 0 ]; then
    echo "test zcompiler failed"
    exit 1
  fi
  
  rm -f $1
}

compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -Werror $inc_dir"
test_once main GCC 

compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic -Werror $inc_dir"
test_once main.exe MinGW wine

compile_string="x86_64-w64-mingw32-g++ main.cpp -o main.exe -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic -Werror $inc_dir"
test_once main.exe MinGW wine

exit 0
