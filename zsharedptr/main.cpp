
#include <zsharedptr>
#include "../test.h"

#if Z_STANDARD_MORE_11
  #include <utility>
#endif

using namespace Z_GLOBAL_NAMESPACE;

class TestTwo
{
public:
  TestTwo(zint16 val1 = 0, zreal val2 = 0.0): val1_(val1), val2_(val2)
  {
    if (val1 == 0)
      throw ZInvalidArgumentException();
  };
  
  zint16 getVal1() const { return val1_; };
  zreal  getVal2() const { return val2_; };
  
private:
  zint16 val1_;
  zreal  val2_; 
};

class TestThree
{
public:
  TestThree(zreal a, zbool b, zint c) : a_(a), b_(b), c_(c) {};
  
  zreal getA() const { return a_; };
  zbool getB() const { return b_; };
  zint  getC() const { return c_; };
   
private:
  zreal a_;
  zbool b_;
  zint  c_;
};

template <typename T, zbool A>
  zuint getSharedCounter(ZSharedPtr<T, A> obj)
{
  return obj.getCounter();
}

template <typename T, zbool A>
  ZSharedPtr<T, A> getSharedPtr()
{  
  T *p = znew<T>();
  ZSharedPtr<T, A> shared(p); 
  
#if Z_STANDARD_MORE_11  
  return std::move(shared);
#else
  return shared;
#endif 
}

bool testZSharedPtr()
{  
  //Базовые тесты "пустого" указателя
  {
    ZSharedPtr<zint> shared;
    TEST(shared.getCounter()==0);
    TEST(shared.isEmpty());
    TEST(shared.getCounter()==0);
    TEST(getSharedCounter(shared)==0);
    TEST(!shared.isArray());
    
    try
    {      
      printf("%d\r\n",static_cast<int>(*shared));
      TEST(false);
    } catch(const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  //Базовые тесты непустого указателя
  {
    zint *p = znew<zint>(15);
    ZSharedPtr<zint> shared(p);
    TEST(shared.getCounter()==1);
    TEST(!shared.isEmpty());
    TEST(!shared.isArray());
    TEST(*shared==15);
    
    shared = shared;
    TEST(shared.getCounter()==1);
    
    ZSharedPtr<zint> shared2;
    shared2 = shared;
    TEST(shared.getCounter()==2);
    TEST(shared2.getCounter()==2);
    TEST(!shared2.isArray());
    TEST(*shared2==15);
  }
  
  //Тесты конструктора перемещения
  {
    ZSharedPtr<zint, false> shared = getSharedPtr<zint,false>();
    TEST(shared.getCounter()==1);
    shared = getSharedPtr<zint,false>();
    TEST(shared.getCounter()==1);
  }

  return true;
}

//Базовые тесты указателя на массив
bool testZSharedPtrArray()
{
  zint *numbers = znewArray<zint>(5);
  ZSharedPtr<zint, true> shared(numbers);
  TEST(getSharedCounter(shared)==2);
  TEST(shared.getCounter()==1);
  TEST(shared.isArray());
    
  try
  {
    printf("%d\r\n",static_cast<int>(*shared));
    TEST(false);
  } catch(const ZInvalidArgumentException&)
  {
    TEST(true);
  }
  
  return true;  
}

bool testzmakeSharedPtr()
{
  //Тест функции zmakeSharedPtr
  {
    ZSharedPtr<zint> shared = zmakeSharedPtr<zint>();
    TEST(shared.getCounter()==1);
  }
  //Тест функции zmakeSharedPtr и оператора разименования указателя
  {
    ZSharedPtr<zint> shared = zmakeSharedPtr<zint>(80);
    TEST(*shared==80);
  }
  //Тест функции zmakeSharedPtr и оператора ->
  {
    ZSharedPtr<TestTwo> shared = zmakeSharedPtr<TestTwo>(15, 18.0);
    TEST(shared->getVal1()==15);
    TEST(shared->getVal2()==18.0);
  }
  
  return true;
}
//Тест функции создания указателя на массив  
bool testzmakeSharedArrayPtr()
{
  ZSharedPtr<zint,true> shared = zmakeSharedArrayPtr<zint>(3);
  TEST(shared.isArray());
  TEST(shared[1]=45);
  return true;
}

bool testConstructors()
{
  //Тест исключения в конструкторе
  {
    try
    {
      ZSharedPtr<TestTwo> shared = zmakeSharedPtr<TestTwo>(0, 18.0);
      TEST(shared->getVal1()==18);
      TEST(false);
    } catch (const ZInvalidArgumentException&)
    {
      TEST(true);
    }    
  }
  
  //Тест конструктора с тремя параметрами
  {
    ZSharedPtr<TestThree> shared = zmakeSharedPtr<TestThree>(4.0, true, 15);
    TEST(shared->getA()==4.0);
    TEST(shared->getB());
    TEST(shared->getC());
  }
  
  return true;
}

//Тестирование оператора ->
bool testOperatorArrow()
{
  //Оператор -> недопустим для "пустого" указателя
  {
    try
    {
      ZSharedPtr<TestTwo> shared;
      TEST(shared->getVal1()==28);
      TEST(false);
    } catch(const ZInvalidArgumentException&)
    {
      TEST(true);
    }    
  }
  
  //Оператор -> недопустим для указателя на массив
  {
    try
    {
      TestTwo *pointer = znewArray<TestTwo>(2);
      ZSharedPtr<TestTwo,true> shared(pointer);
      TEST(shared->getVal1()==15);
      TEST(false);
    } catch(const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  return true;
}

//Тестирование оператора []
bool testOperatorBrackets()
{
  //Тест оператора []
  {
    zint *numbers = znewArray<zint>(3);
    numbers[0] = 15;
    numbers[1] = 56;
    numbers[2] = -8;
    ZSharedPtr<zint, true> shared(numbers);
    TEST(shared[0]==15);
    TEST(shared[1]==56);
    TEST(shared[2]==-8);
    
    shared[1]=18;
    TEST(shared[1]==18);
  }
  
  //Оператор [] недопустим для пустых указателей
  {
    try
    {
      ZSharedPtr<zint, true> shared;
      TEST(shared[0]==0);
      TEST(false);
    } catch (const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  //Оператор [] недопустим для не-массивов
  {
    try
    {
      ZSharedPtr<zint> shared = zmakeSharedPtr<zint>(18);
      TEST(shared[0]==18);
      TEST(false);      
    } catch (const ZInvalidArgumentException&)
    {
      TEST(true);
    }
  }
  
  return true;
}

bool testOperators()
{
  return ( testOperatorArrow() && testOperatorBrackets() );
}

bool test()
{
  return (testZSharedPtr() && testZSharedPtrArray() && testzmakeSharedPtr() &&
          testzmakeSharedArrayPtr() && testConstructors() && testOperators() );
}

int main()
{
  return ( test() ? 0 : 1 );
}
