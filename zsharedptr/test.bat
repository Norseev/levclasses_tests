@echo off

echo "start testing zsharedptr"

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++98 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zsharedptr failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zsharedptr failed"
  exit /b 1
)

del main.exe

g++ main.cpp -o main.exe -Wall -Wextra -pedantic -std=c++11 %inc_dir% 
if ERRORLEVEL 1 (
  echo "test zsharedptr failed"
  exit /b 1
)

call main.exe
if ERRORLEVEL 1 (
  echo "test zsharedptr failed"
  exit /b 1
)

del main.exe

exit /b 0