#!/bin/sh

echo "start testing zsharedptr"

# $1 - имя скомпилированного файла
# $2 - утилита wine, если производится компиляция под Windows
test_once()
{
  rm -f $1
  $compile_string

  if [ $? != 0 ]; then
    echo "test zsharedptr failed"
    exit 1
  fi

  $2 ./$1
  if [ $? != 0 ]; then
    echo "test zsharedptr failed"
    exit 1
  fi 
}

#Linux 64-bit
compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -std=c++98 $inc_dir"
test_once main
if [ $? != 0 ]; then
  exit 1
fi

compile_string="g++ main.cpp -o main -Wall -Wextra -pedantic -std=c++11 $inc_dir"
test_once main
if [ $? != 0 ]; then
  exit 1
fi

#Linux 32 bit
compile_string="g++ main.cpp -o main -m32 -Wall -Wextra -pedantic -std=c++98 $inc_dir"
test_once main
if [ $? != 0 ]; then
  exit 1
fi

compile_string="g++ main.cpp -o main -m32 -Wall -Wextra -pedantic -std=c++11 $inc_dir"
test_once main
if [ $? != 0 ]; then
  exit 1
fi

rm -f main

#Windows
compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++98 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi

compile_string="i686-w64-mingw32-g++ main.cpp -o main.exe -std=c++11 -static-libgcc -static-libstdc++ -Wall -Wextra -pedantic $inc_dir"
test_once main.exe wine
if [ $? != 0 ]; then
  exit 1
fi

rm -f main.exe

exit 0
